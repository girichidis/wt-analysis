# plot initial conditions
# Philipp Girichidis, 2020-03

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import ImageGrid
import numpy as np
import yt
from matplotlib.ticker import LogLocator
from matplotlib.colors import LogNorm
from matplotlib import rc
from matplotlib import rcParams
rc('text', usetex=True)
rcParams['text.latex.preamble'] = [r'\boldmath']
rc('font', family='serif')

import matplotlib.cm as cm

from mpl_toolkits.axes_grid1 import make_axes_locatable
from mpl_toolkits.axes_grid1.colorbar import colorbar

import argparse
import os


pc = 3.085e18
resx = 1280
resy =  256
resz =  256

lvmin = [1e-25, 0.0, -1e6, -1e6, -1e6, -7, -1e-5, -1e-5, -1e-5, 0.0]
lvmax = [3e-25, 1.5,  1e6,  1e6,  1e6, -5,  1e-5,  1e-5,  1e-5, 1.5]

for sim in ["done-L6-M00-d010-ng"]: #, "done-L6-M6x-d010-ng", "done-L6-M6y-d010-ng"]:
    
    ds = yt.load(sim+"/wind_hdf5_plt_cnt_0000")
    extent1 = [ds.domain_left_edge[1].in_units("pc"),ds.domain_right_edge[1].in_units("pc"),ds.domain_left_edge[2].in_units("pc"),ds.domain_right_edge[2].in_units("pc")]

    cmap_dens = cm.RdYlBu_r
    cmap_velo = cm.inferno
    cmap_magn = cm.plasma

    data = []
    # get density, velocity, magnetic field
    for field in ["dens", "velocity_magnitude", "velx", "vely", "velz", "magnetic_field_strength", "magx", "magy", "magz"]:
        dat=yt.SlicePlot(ds, 'z', field, center=[0,0,0]).data_source.to_frb(ds.domain_width[1], (resy, resz), height=ds.domain_width[2])[field]
        data.append(dat)
    data.append(np.sqrt(np.square(data[2]) + np.square(data[3]) + np.square(data[4])))

    fig, axes = plt.subplots(ncols=3, sharey=True, figsize=(8,4))

    im0 = axes[0].imshow(         data[0].T,      origin="lower", aspect=1.0, extent=extent1, vmin=lvmin[0], vmax=lvmax[0], cmap=cmap_dens)
    im1 = axes[1].imshow(         data[9].T/1e5,  origin="lower", aspect=1.0, extent=extent1, vmin=lvmin[9], vmax=lvmax[9], cmap=cmap_velo)
    im2 = axes[2].imshow(np.log10(data[5]).T,     origin="lower", aspect=1.0, extent=extent1, vmin=lvmin[5], vmax=lvmax[5], cmap=cmap_magn)

    # add wiggles to the plots
    Nwig=40
    amplitude=0.03*(extent1[3].v-extent1[2].v)
    Ypts = np.linspace(extent1[0].v,extent1[1].v,Nwig,endpoint=True)
    Xpts = extent1[-1].v * np.ones(Nwig)
    Xpts[1::2] += amplitude
    Xpts[0::2] -= amplitude
    axes[0].plot(Xpts,Ypts,linewidth=4,solid_joinstyle='miter',color=cmap_dens(0.0))
    axes[0].spines["right"].set_visible(False)
    axes[1].plot(Xpts,Ypts,linewidth=4,solid_joinstyle='miter',color=cmap_velo(1.0))
    axes[1].spines["right"].set_visible(False)
    axes[2].plot(Xpts,Ypts,linewidth=0.5,solid_joinstyle='miter',color="k", linestyle="dotted")
    axes[2].spines["right"].set_visible(False)
    
    # add wind arrow
    axes[0].arrow(-90,60,50,0, head_width=5, head_length=5, fc='w', ec='w', linewidth=2)
    axes[0].text(-90,75, "$v_\\mathrm{w}=100\\,\\mathrm{km\\,s}^{-1}$", color="white")

    aspect0 = axes[0].get_aspect()
    axes[0].set_ylabel("$z~(\\mathrm{pc})$")
    axes[0].set_xlabel("$x~(\\mathrm{pc})$")
    axes[1].set_xlabel("$x~(\\mathrm{pc})$")
    axes[2].set_xlabel("$x~(\\mathrm{pc})$")

    ticks   = [1e-25, 2e-25, 3e-25]
    tickpos = (np.array(ticks)-lvmin[0])/(lvmax[0]-lvmin[0])
    print("Tick positions in linear space from 0 to 1:", tickpos)
    ax0_divider = make_axes_locatable(axes[0])
    cax0 = ax0_divider.append_axes("top", size="7%", pad="15%")
    cb0  = matplotlib.colorbar.ColorbarBase(cax0, cmap=cmap_dens, orientation="horizontal", extend='min', \
                                            ticks=tickpos)
    cax0.xaxis.set_ticks_position("bottom")
    cax0.xaxis.set_label_position("top")
    cb0.ax.set_xlabel("\\textbf{density ($10^{-25}$ g cm$^{-3}$)}")
    #cb0.set_ticks([1e-25, 2e-25, 3e-25])
    #cb0.ax.set_xticklabels(["$10^{-25}$","$2\\times10^{-25}$", "$3\\times10^{-25}$"])
    cb0.ax.set_xticklabels(["$1$","$2$", "$3$"])

    ax1_divider = make_axes_locatable(axes[1])
    cax1 = ax1_divider.append_axes("top", size="7%", pad="15%")
    cb1  = matplotlib.colorbar.ColorbarBase(cax1, cmap=cmap_velo, orientation="horizontal", extend='max', ticks=[0.0, 0.5, 1.0, 1.5])
    cax1.xaxis.set_ticks_position("bottom")
    cax1.xaxis.set_label_position("top")
    cb1.ax.set_xlabel("\\textbf{velocity (km~s$^{-1}$)}")
    #cax1.set_xticks([0.0, 0.5, 1.0, 1.5])

    ax2_divider = make_axes_locatable(axes[2])
    cax2 = ax2_divider.append_axes("top", size="7%", pad="15%")
    cb2  = colorbar(im2, cax=cax2, orientation="horizontal")
    cax2.xaxis.set_ticks_position("bottom")
    cax2.xaxis.set_label_position("top")
    cb2.ax.set_xlabel("\\textbf{log[ B field ($\\mu$G) ]}")
    cax2.set_xticks([-7, -6, -5])

    print("save plots as: plots/ICs/"+sim+"-IC-dens-velo-magn.{png,pdf}")

    fig.savefig("plots/ICs/"+sim+"-IC-dens-velo-magn.png", bbox_inches="tight")
    fig.savefig("plots/ICs/"+sim+"-IC-dens-velo-magn.pdf", bbox_inches="tight")
