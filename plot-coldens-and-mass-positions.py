# plot density slices and streamlines

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import ImageGrid
import numpy as np
import yt
from matplotlib.ticker import LogLocator
from matplotlib.colors import LogNorm
from matplotlib import rc
from matplotlib import rcParams
rc('text', usetex=True)
rcParams['text.latex.preamble'] = [r'\usepackage{bm}\usepackage{lmodern}\boldmath']
rc('font', family='serif')

from mpl_toolkits.axes_grid1 import make_axes_locatable
from mpl_toolkits.axes_grid1.colorbar import colorbar

import argparse
import os

plot_wind_comparison = True
plot_time = False

cmap="bone"

pc = 3.085e18
resx = 1280
resy =  256
resz =  256

resxs = resx//2
resys = resy//2
reszs = resz//2

fraction=[0.25, 0.4, 0.5]
colors  =["red", "cyan", "gold", "white"]
text = ["25 percentile", "40 percentile", "50 percentile", "centre of mass"]
picposy=[70, 35, 0, -35]

if plot_wind_comparison == True:

    for c_dens in [ "d010", "d050", "d100"]:
        if c_dens=="d010":
            times = [ "0050", "0100", "0150" ]
        if c_dens=="d050":
            times = [ "0100", "0200", "0300" ]
        if c_dens=="d100":
            times = [ "0200", "0400"]

        winds = ["M00", "M6x", "M6y"]

        print(times)

        for i in range(len(times)):
            data = []
            positions = []
            extent1 = [0., 0., 0., 0.]

            print(times[i])
            print(i)
            for wind in winds:
                sim="done-L6-"+wind+"-"+c_dens+"-sg"
                print(sim)
                f1 = sim+"/wind_hdf5_plt_cnt_"+times[i]
                ds1 = yt.load(f1)
                if not os.path.isfile(sim+"/cold-"+times[i]+".npy"):
                    dens1 = np.array(yt.ProjectionPlot(ds1, 'z', "density").data_source.to_frb(ds1.domain_width[0], (resx, resy), height=ds1.domain_width[1])["density"])
                    np.save(sim+"/cold-"+times[i]+".npy", dens1)
                else:
                    dens1=np.load(sim+"/cold-"+times[i]+".npy")
                data.append(dens1)

                # load precomputed profile file
                prf  = np.loadtxt(sim+"/wind_hdf5_plt_cnt_"+times[i]+"-all-profiles.dat")
                # get total mass of the profile
                Mtot = prf[:,1].sum()
                # the centre of mass
                CoMx = (prf[:,0]*prf[:,1]).sum()/Mtot
                # and the cumulative mass profile
                cum_rel_mass = prf[:,1].cumsum()/Mtot
                # create array with positions
                # first N are the cumulative mass fractions
                position=[0.0]*(1+len(fraction))
                for f, ix in zip(fraction, range(len(fraction))):
                    idx = np.argwhere(cum_rel_mass > f)[0]
                    position[ix] = prf[idx,0][0]
                    # last entry is centre of mass
                position[len(fraction)] = CoMx
                # append positions to global array
                positions.append(position)

                extent1[0] = ds1.domain_left_edge[0].in_units("pc")
                extent1[1] = ds1.domain_right_edge[0].in_units("pc")
                extent1[2] = ds1.domain_left_edge[1].in_units("pc")
                extent1[3] = ds1.domain_right_edge[1].in_units("pc")
        
            #lvmin = [-28]
            #lvmax = [-22]
            lvmin = [-6]
            lvmax = [-2]
        
            Y, X = np.mgrid[extent1[2]:extent1[3]:resys*1j, extent1[0]:extent1[1]:resxs*1j]
            
            fig = plt.figure(figsize=(8,10))
            axes = ImageGrid(fig, 111, (3, 1), axes_pad=0.15, aspect=True, share_all=False, cbar_location="top", cbar_mode="single", cbar_pad=0.3, cbar_size="8%")
            
            ims = []
            for it in range(len(winds)):
                ims.append(axes[it].imshow(np.log10(data[it]),     aspect=1.0, extent=extent1, vmin=lvmin[0], vmax=lvmax[0], cmap=cmap))
                axes[it].set_xlim(extent1[0],extent1[1])
                axes[it].set_ylim(extent1[2],extent1[3])
                for j in range(len(positions[it])):
                    print("position: ", j, " color: ", colors[j])
                    axes[it].axvline(positions[it][j]/pc, color=colors[j])
                    
            cax = axes.cbar_axes[0]
            cbar = cax.colorbar(ims[0], ticks=np.arange(lvmin[0], lvmax[0]+1))
            cax.set_xlabel("\\textbf{log[ column density (g cm$^{-2}$) ]}")
            cax.xaxis.set_ticks_position('bottom')
            
            # create legend
            for ic in range(len(colors)):
                axes[0].text(550,picposy[ic], text[ic], color=colors[ic])
            
            fig.savefig("plots/mass-positions/"+c_dens+"-"+times[i]+"-mass-positions.png", bbox_inches="tight")
            fig.savefig("plots/mass-positions/"+c_dens+"-"+times[i]+"-mass-positions.pdf", bbox_inches="tight")


if plot_time == True:

    times=["0100", "0200", "0300", "0400"]

    for sim in ["done-L6-M00-d010-sg", "done-L6-M6x-d010-sg", "done-L6-M6y-d010-sg", "done-L6-M00-d100-sg", "done-L6-M6x-d100-sg", "done-L6-M6y-d100-sg"]:
        print("simulation: ", sim)
        
        data = []
        simtimes = []
        positions = []
        extent1 = [0., 0., 0., 0.]
        
        for i in range(len(times)):
            print("  time: ", times[i])
            f1=sim+"/wind_hdf5_plt_cnt_"+times[i]
            ds1 = yt.load(f1)
            if not os.path.isfile(sim+"/cold-"+times[i]+".npy"):
                dens1 = np.array(yt.ProjectionPlot(ds1, 'z', "density").data_source.to_frb(ds1.domain_width[0], (resx, resy), height=ds1.domain_width[1])["density"])
                np.save(sim+"/cold-"+times[i]+".npy", dens1)
            else:
                dens1=np.load(sim+"/cold-"+times[i]+".npy")
            data.append(dens1)

            # load precomputed profile file
            prf  = np.loadtxt(sim+"/wind_hdf5_plt_cnt_"+times[i]+"-all-profiles.dat")
            # get total mass of the profile
            Mtot = prf[:,1].sum()
            # the centre of mass
            CoMx = (prf[:,0]*prf[:,1]).sum()/Mtot
            # and the cumulative mass profile
            cum_rel_mass = prf[:,1].cumsum()/Mtot
            # create array with positions
            # first N are the cumulative mass fractions
            position=[0.0]*(1+len(fraction))
            for f, ix in zip(fraction, range(len(fraction))):
                idx = np.argwhere(cum_rel_mass > f)[0]
                position[ix] = prf[idx,0][0]
                # last entry is centre of mass
            position[len(fraction)] = CoMx
            # append positions to global array
            positions.append(position)

            extent1[0] = ds1.domain_left_edge[0].in_units("pc")
            extent1[1] = ds1.domain_right_edge[0].in_units("pc")
            extent1[2] = ds1.domain_left_edge[1].in_units("pc")
            extent1[3] = ds1.domain_right_edge[1].in_units("pc")
            
        #lvmin = [-28]
        #lvmax = [-22]
        lvmin = [-7]
        lvmax = [-1]
        
        Y, X = np.mgrid[extent1[2]:extent1[3]:resys*1j, extent1[0]:extent1[1]:resxs*1j]
        
        fig = plt.figure(figsize=(8,10))
        axes = ImageGrid(fig, 111, (4, 1), axes_pad=0.15, aspect=True, share_all=False, cbar_location="top", cbar_mode="single", cbar_pad=0.3)
        
        ims = []
        for i in range(len(times)):
            ims.append(axes[i].imshow(np.log10(data[i]),     aspect=1.0, extent=extent1, vmin=lvmin[0], vmax=lvmax[0], cmap=cmap))
            axes[i].set_xlim(extent1[0],extent1[1])
            axes[i].set_ylim(extent1[2],extent1[3])
            for j in range(len(positions[i])):
                print("position: ", j, " color: ", colors[j])
                axes[i].axvline(positions[i][j]/pc, color=colors[j])

        cax = axes.cbar_axes[0]
        cbar = cax.colorbar(ims[0], ticks=np.arange(lvmin[0], lvmax[0]+1))
        cax.set_xlabel("\\textbf{log[ column density (g cm$^{-2}$) ]}")
        cax.xaxis.set_ticks_position('bottom')
        
        # create legend
        for ic in range(len(colors)):
            axes[0].text(550,picposy[ic], "\\\textbf{"+text[ic]+"}", color=colors[ic])
            
        fig.savefig("plots/mass-positions/"+sim+"-mass-positions.png", bbox_inches="tight")
        fig.savefig("plots/mass-positions/"+sim+"-mass-positions.pdf", bbox_inches="tight")
