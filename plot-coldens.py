# script to plot the column densities in various combinations
# Philipp Girichidis, 2019, 2020
# 2020-11-13: changed chemistry plots
# 2021-02-08: added comparison between winds (for vids)

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import ImageGrid
import numpy as np
import yt
from matplotlib.ticker import LogLocator
from matplotlib.colors import LogNorm
from matplotlib import rc
from matplotlib import rcParams
rc('text', usetex=True)
rcParams['text.latex.preamble'] = [r'\boldmath \usepackage{bm}']
rc('font', family='serif')

from mpl_toolkits.axes_grid1 import make_axes_locatable

import argparse
import os



def get_output_var(outdir, outname, args):
    if args.oname == "":
        oname = outname
    else:
        oname = args.oname

    if args.odir == "":
        odir = outdir
    else:
        odir = args.odir

    return odir, oname


def save_picture(odir, oname, fig, args):
    print("saving plot: ", odir+"/"+oname+".png")
    fig.savefig(odir+"/"+oname+".png", bbox_inches="tight")
    if args.pdf:
        print("saving plot: ", odir+"/"+oname+".pdf")
        fig.savefig(odir+"/"+oname+".pdf", bbox_inches="tight")



pc = 3.085e18
resx = 1280
resy =  256
Sigma_to_N = 4.23728813559322e+23

cmap_cd="RdYlBu_r"

# Command line arguments
parser = argparse.ArgumentParser(description='coldens plots')
#parser.add_argument('files', nargs='+', help='FLASH data files')
parser.add_argument('-field', '-f', type=str, default='density')
parser.add_argument('-resx', type=int, default=1024)
parser.add_argument('-resy', type=int, default=1024)
parser.add_argument('-force', action="store_true")
parser.add_argument('-pdf', action="store_true")

parser.add_argument('-chem_only', action="store_true", help="omits total column density in plots")

parser.add_argument('-oname',    default="", type=str)
parser.add_argument('-odir' ,    default="", type=str)

# plotting mode
#  "time"      time evolution of a simulation
#  "chem"      chem. composition, old color maps
#  "prof"      density and profiles along x
#  "slice-ov"  slice with overview quantities
#  "tcc"       coldens at 1.25 and 2.5 tcc
#  "tcc25chem" chemistry at 2.5 tcc
#  "tcc25CO"   chemistry at sim 2.5 tcc (iha, ih2, ico)
#  "CD_warm_ion" coldens of warm ionized medium
#  "CD_3_comp"   coldens of three sims at same time
#                in this mode sue the following -sim params
#                -sim d{010,050,100}-{ng,sg}  : compare all d010-sg
#  "CD_chem_comp"  coldens of WIM, Ha, H2 for one sim
#
#         give -sim simfolder and -plt plt_numbers
#
parser.add_argument('-mode', type=str, default="time")

parser.add_argument('-sim', type=str, nargs='+')
parser.add_argument('-plt', type=str, nargs='+')

args = parser.parse_args()


valmin=1e-6
valmax=1e-2

lvmin=np.log10(valmin)
lvmax=np.log10(valmax)

figsize_x = 10.0
figsize_y =  1.3

if args.mode == "slice-ov":

    for sim in args.sim:
        print("rendering time evolution for simulation: ", sim)
        for pltcnt in args.plt:
            pltfmt=pltcnt.zfill(4)
            print("using time snapshots:", pltfmt)
            
            file = sim+"/wind_hdf5_plt_cnt_"+pltfmt

            ds = yt.load(file)
            extent1 = [ds.domain_left_edge[0].in_units("pc"),ds.domain_right_edge[0].in_units("pc"),ds.domain_left_edge[1].in_units("pc"),ds.domain_right_edge[1].in_units("pc")]

            # load column density file
            coldens = np.load(file+"-projection-z-1280x0256.npy")
            prf     = np.loadtxt(file+"-all-profiles.dat")
            
            fields     = ["density", "temperature", "pressure", "mach_number",    "magnetic_field_strength", "plasma_beta"]
            names      = ["density", "temperature", "pressure", "Mach number",    "mag. field strength"    , "plasma beta"]
            symbol     = ["$\\rho$", "$T$",         "$P$",      "$\\mathcal{M}$", "$B$"                    , "$\\beta$"]
            cmaps      = ["plasma",  "jet",         "afmhot",   "coolwarm",       "plasma"                 , "coolwarm"]
            mins       = [-27,       1,             -15,        -1,               -7,                        -1]
            maxs       = [-22,       6,             -12,        1,                -4,                        1]
        
            fig, axes = plt.subplots(figsize=(10,figsize_y*len(fields)), nrows=len(fields), sharex=True)

            for f, n, s, c, i in zip(fields, names, symbol, cmaps, range(len(fields))):
                data = np.array(yt.SlicePlot(ds, 'z', f, center=ds.domain_center).data_source.to_frb(ds.domain_width[0], (resx, resy), height=ds.domain_width[1])[f]).T
                print(f, "min:", np.min(data), " max:", np.max(data))
                im = axes[i].imshow(np.log10(data).T, aspect=1.0, extent=extent1, cmap=c, vmin=mins[i], vmax=maxs[i])
                divider = make_axes_locatable(axes[i])
                cax = divider.append_axes('right', size='5%', pad=0.05)
                plt.colorbar(im, cax=cax, orientation='vertical')
                

            odir, oname = get_output_var("plots/slices", sim+"-"+pltfmt+"-slices-overview", args)
            save_picture(odir, oname, fig, args)


if args.mode == "prof":
    
    for sim in args.sim:
        print("rendering time evolution for simulation: ", sim)
    
        for pltcnt in args.plt:
            pltfmt=pltcnt.zfill(4)
            print("using time snapshots:", pltfmt)

            file = sim+"/wind_hdf5_plt_cnt_"+pltfmt

            # check whether file exists
            fname = file+"-all-profiles.dat"
            if not os.path.isfile(fname):
                print("profile file ", fname, " does not exists! Run profile analysis first!")
                exit()

            ds = yt.load(file)
            extent1 = [ds.domain_left_edge[0].in_units("pc"),ds.domain_right_edge[0].in_units("pc"),ds.domain_left_edge[1].in_units("pc"),ds.domain_right_edge[1].in_units("pc")]

            # load column density file
            coldens = np.load(file+"-projection-z-1280x0256.npy")
            prf     = np.loadtxt(file+"-all-profiles.dat")
            
            fig, axes = plt.subplots(nrows=3, sharex=True)
            
            axes[0].imshow(np.log10(coldens).T, aspect=1.0, extent=extent1, vmin=lvmin, vmax=lvmax, cmap=cmap_cd)
            # plot vertical line for certain percentiles of mass
            Mtot = prf[:,1].sum()
            cum_rel_mass = prf[:,1].cumsum()/Mtot
            #print(cum_rel_mass[0], cum_rel_mass[-1])
            
            offsets=[-55, 5, 5]
            fraction=[0.5, 0.75, 0.9]
            textinfo=["50\\%", "75\\%", "90\\%"]
            for f, offset, info in zip(fraction, offsets, textinfo):
                idx = np.argwhere(cum_rel_mass > f)[0]
                pos = prf[idx,0]/pc
                #print(idx, pos/pc)
                axes[0].axvline(pos, color="white")
                axes[0].text(pos+offset,70,info, color="white")

            aspect0 = axes[0].get_aspect()
            axes[0].set_ylabel("$y~(\\mathrm{pc})$")

            axes[1].semilogy(prf[:,0]/pc, prf[:, 3]*1e24, label="$\\rho~(10^{-24}\\,\\mathrm{g\\,cm^{-3}})$")
            axes[1].semilogy(prf[:,0]/pc, prf[:, 7]*1e24, label="$\\rho_\\mathrm{H_2}~(10^{-24}\\,\\mathrm{g\\,cm^{-3}})$")
            axes[1].semilogy(prf[:,0]/pc, prf[:,17],      label="$T~\\mathrm{(K)}$")
            axes[1].set_ylabel("$T,\\,\\rho,\\,\\rho_{\\mathrm{H_2}}$")
            axes[1].legend(loc="lower right", framealpha=1.0)
            axes[1].set_yticks([1e-5, 1e-3, 1e-1, 1e1, 1e3, 1e5, 1e7])
            axes[1].set_ylim(1e-5, 1e7)
            
            axes[2].semilogy(prf[:,0]/pc, prf[:,11]/1e5, label="$\\sigma_{yz}~(\\mathrm{km\\,s}^{-1})$")
            axes[2].semilogy(prf[:,0]/pc, prf[:,16]*1e6, label="$|\\mathbf{B}|~(\\mu\\mathrm{G})$")
            axes[2].set_yticks([0.1, 1.0, 10])
            axes[2].set_ylim(0.1, 10)

            axes[2].set_xlim(ds.domain_left_edge[0].in_units("pc"), ds.domain_right_edge[0].in_units("pc"))
            axes[2].set_xlabel("$x~(\\mathrm{pc})$")
            axes[2].set_ylabel("$\\sigma_{yz},\\,|\\mathbf{B}|$")
            axes[2].legend(loc="lower right", framealpha=1.0)
            
            odir, oname = get_output_var("plots/profiles", sim+"-"+pltfmt+"-cd-profiles", args)
            save_picture(odir, oname, fig, args)


if args.mode == "chem":

    for sim in args.sim:

        print("rendering time evolution for simulation: ", sim)

        for pltcnt in args.plt:
            pltfmt=pltcnt.zfill(4)
            print("using time snapshots:", pltfmt)

            # parameter
            if args.chem_only:
                Nfields=3
            else:
                Nfields=4

            ref_file = sim+"/wind_hdf5_plt_cnt_"+args.plt[0].zfill(4)
            print("loading reference file:", ref_file)
            ds = yt.load(ref_file)
            extent1 = [ds.domain_left_edge[0].in_units("pc"),ds.domain_right_edge[0].in_units("pc"),ds.domain_left_edge[1].in_units("pc"),ds.domain_right_edge[1].in_units("pc")]
            extent2 = [ds.domain_left_edge[1].in_units("pc"),ds.domain_right_edge[1].in_units("pc"),ds.domain_left_edge[2].in_units("pc"),ds.domain_right_edge[2].in_units("pc")]

            # simulation files
            yfiles = []
            xfiles = []
            title  = []
            ims    = []

            if not args.chem_only:
                # also plot the total column density
                print("loading data:", sim+"/wind_hdf5_plt_cnt_"+pltfmt+"-projection-z-1280x0256.npy")
                print("loading data:", sim+"/wind_hdf5_plt_cnt_"+pltfmt+"-projection-x-1280x0256.npy")
                yfiles.append(sim+"/wind_hdf5_plt_cnt_"+pltfmt+"-projection-z-1280x0256.npy")
                xfiles.append(sim+"/wind_hdf5_plt_cnt_"+pltfmt+"-projection-x-0256x0256.npy")
                title.append("total gas")
            
            for chem, name in zip(["denshp", "densha", "densh2"],["ionized", "atomic", "molecular"]):
                print("loading data:", sim+"/wind_hdf5_plt_cnt_"+pltfmt+"-projection-z-1280x0256-"+chem+".npy")
                print("loading data:", sim+"/wind_hdf5_plt_cnt_"+pltfmt+"-projection-x-1280x0256-"+chem+".npy")
                yfiles.append(sim+"/wind_hdf5_plt_cnt_"+pltfmt+"-projection-z-1280x0256-"+chem+".npy")
                xfiles.append(sim+"/wind_hdf5_plt_cnt_"+pltfmt+"-projection-x-0256x0256-"+chem+".npy")
                title.append(name)
                
            fig = plt.figure(figsize=(figsize_x, figsize_y*Nfields))
            grid = ImageGrid(fig, 111, (Nfields, 2), axes_pad=0.15, aspect=True, share_all=False, cbar_location="top", cbar_mode="single", cbar_pad=0.3)

            # fill images
            for i in range(Nfields):
                ydata = np.log10(np.load(yfiles[i]))
                ims.append(grid[2*i].imshow(ydata.T, aspect=1.0, extent=extent1, vmin=lvmin, vmax=lvmax, cmap=cmap_cd))
                grid[2*i].text(-80,70, title[i], color="white")
                xdata = np.log10(np.load(xfiles[i]))
                ims.append(grid[2*i+1].imshow(xdata.T, aspect=1.0, extent=extent2, vmin=lvmin, vmax=lvmax, cmap=cmap_cd))
                
                grid[2*i].set_ylabel("$y~~(\\mathrm{pc})$")

            # add axes and colorbars
            grid[2*Nfields-2].set_xlabel("$x~~(\\mathrm{pc})$")
            grid[2*Nfields-1].set_xlabel("$z~~(\\mathrm{pc})$")
            cax = grid.cbar_axes[0]
            cbar = cax.colorbar(ims[-1], ticks=np.arange(lvmin, lvmax+1))
            cax.set_xlabel("\\textbf{log[ column density (g cm$^{-2}$) ]}")
            cax.xaxis.set_ticks_position('bottom')

            if args.chem_only:
                odir, oname = get_output_var("plots/coldens", "chem-"+sim+"-"+pltfmt, args)
            else:
                odir, oname = get_output_var("plots/coldens", "cd-chem-"+sim+"-"+pltfmt, args)
            save_picture(odir, oname, fig, args)


if args.mode == "tcc":
    print("generate a plot at 1.25 and 2.5 tcc")

    for cloud in [ "d010", "d050", "d100" ]:

        ds = yt.load("done-L6-M00-d010-sg/wind_hdf5_plt_cnt_0000")
        extent1 = [ds.domain_left_edge[0].in_units("pc"),ds.domain_right_edge[0].in_units("pc"),ds.domain_left_edge[1].in_units("pc"),ds.domain_right_edge[1].in_units("pc")]
        extent2 = [ds.domain_left_edge[1].in_units("pc"),ds.domain_right_edge[1].in_units("pc"),ds.domain_left_edge[2].in_units("pc"),ds.domain_right_edge[2].in_units("pc")]

        fig = plt.figure(figsize=(8,8))
        
        Nimg=6
        
        lmargin=0.1
        bmargin=0.1
        sizex=0.6
        sizey=sizex/5.
        xpad=0.01
        ypad=0.01
        ypad2=0.05
        
        sizex_cbar_h = sizex+xpad+sizey
        sizey_cbar_h = 0.02
        sizex_cbar_v = 0.02
        sizey_cbar_v = Nimg*sizey + (Nimg-1)*ypad
        xpad_cbar = 0.025
        ypad_cbar = 0.01
        
        axes = []
        xpos = lmargin
        ypos = bmargin
        for g in range(3):
            for i in range(2):
                print(xpos, ypos, sizex, sizey, xpos+sizex+xpad+sizey)
                axes.append(plt.axes([xpos,            ypos, sizex, sizey]))
                axes.append(plt.axes([xpos+sizex+xpad, ypos, sizey, sizey]))
                ypos = ypos + sizey + ypad
            ypos = ypos + ypad2

        # pictures arranges from bottom to top

        if cloud == "d010":
            data = []
            data.append(np.load("done-L6-M6y-d010-sg/wind_hdf5_plt_cnt_0122-projection-z-1280x0256.npy"))
            data.append(np.load("done-L6-M6y-d010-sg/wind_hdf5_plt_cnt_0122-projection-x-0256x0256.npy"))
            data.append(np.load("done-L6-M6y-d010-sg/wind_hdf5_plt_cnt_0061-projection-z-1280x0256.npy"))
            data.append(np.load("done-L6-M6y-d010-sg/wind_hdf5_plt_cnt_0061-projection-x-0256x0256.npy"))
            data.append(np.load("done-L6-M6x-d010-sg/wind_hdf5_plt_cnt_0122-projection-z-1280x0256.npy"))
            data.append(np.load("done-L6-M6x-d010-sg/wind_hdf5_plt_cnt_0122-projection-x-0256x0256.npy"))
            data.append(np.load("done-L6-M6x-d010-sg/wind_hdf5_plt_cnt_0061-projection-z-1280x0256.npy"))
            data.append(np.load("done-L6-M6x-d010-sg/wind_hdf5_plt_cnt_0061-projection-x-0256x0256.npy"))
            data.append(np.load("done-L6-M00-d010-sg/wind_hdf5_plt_cnt_0122-projection-z-1280x0256.npy"))
            data.append(np.load("done-L6-M00-d010-sg/wind_hdf5_plt_cnt_0122-projection-x-0256x0256.npy"))
            data.append(np.load("done-L6-M00-d010-sg/wind_hdf5_plt_cnt_0061-projection-z-1280x0256.npy"))
            data.append(np.load("done-L6-M00-d010-sg/wind_hdf5_plt_cnt_0061-projection-x-0256x0256.npy"))
            time1 = "6.1"
            time2 = "12.2"
            dens = "0.1"

        if cloud == "d050":
            data = []
            data.append(np.load(     "L6-M6y-d050-sg/wind_hdf5_plt_cnt_0273-projection-z-1280x0256.npy"))
            data.append(np.load(     "L6-M6y-d050-sg/wind_hdf5_plt_cnt_0273-projection-x-0256x0256.npy"))
            data.append(np.load(     "L6-M6y-d050-sg/wind_hdf5_plt_cnt_0137-projection-z-1280x0256.npy"))
            data.append(np.load(     "L6-M6y-d050-sg/wind_hdf5_plt_cnt_0137-projection-x-0256x0256.npy"))
            data.append(np.load("done-L6-M6x-d050-sg/wind_hdf5_plt_cnt_0273-projection-z-1280x0256.npy"))
            data.append(np.load("done-L6-M6x-d050-sg/wind_hdf5_plt_cnt_0273-projection-x-0256x0256.npy"))
            data.append(np.load("done-L6-M6x-d050-sg/wind_hdf5_plt_cnt_0137-projection-z-1280x0256.npy"))
            data.append(np.load("done-L6-M6x-d050-sg/wind_hdf5_plt_cnt_0137-projection-x-0256x0256.npy"))
            data.append(np.load("done-L6-M00-d050-sg/wind_hdf5_plt_cnt_0273-projection-z-1280x0256.npy"))
            data.append(np.load("done-L6-M00-d050-sg/wind_hdf5_plt_cnt_0273-projection-x-0256x0256.npy"))
            data.append(np.load("done-L6-M00-d050-sg/wind_hdf5_plt_cnt_0137-projection-z-1280x0256.npy"))
            data.append(np.load("done-L6-M00-d050-sg/wind_hdf5_plt_cnt_0137-projection-x-0256x0256.npy"))
            time1 = "13.7"
            time2 = "27.3"
            dens = "0.5"

        if cloud == "d100":
            data = []
            data.append(np.load("done-L6-M6y-d100-sg/wind_hdf5_plt_cnt_0387-projection-z-1280x0256.npy"))
            data.append(np.load("done-L6-M6y-d100-sg/wind_hdf5_plt_cnt_0387-projection-x-0256x0256.npy"))
            data.append(np.load("done-L6-M6y-d100-sg/wind_hdf5_plt_cnt_0193-projection-z-1280x0256.npy"))
            data.append(np.load("done-L6-M6y-d100-sg/wind_hdf5_plt_cnt_0193-projection-x-0256x0256.npy"))
            data.append(np.load("done-L6-M6x-d100-sg/wind_hdf5_plt_cnt_0387-projection-z-1280x0256.npy"))
            data.append(np.load("done-L6-M6x-d100-sg/wind_hdf5_plt_cnt_0387-projection-x-0256x0256.npy"))
            data.append(np.load("done-L6-M6x-d100-sg/wind_hdf5_plt_cnt_0193-projection-z-1280x0256.npy"))
            data.append(np.load("done-L6-M6x-d100-sg/wind_hdf5_plt_cnt_0193-projection-x-0256x0256.npy"))
            data.append(np.load("done-L6-M00-d100-sg/wind_hdf5_plt_cnt_0387-projection-z-1280x0256.npy"))
            data.append(np.load("done-L6-M00-d100-sg/wind_hdf5_plt_cnt_0387-projection-x-0256x0256.npy"))
            data.append(np.load("done-L6-M00-d100-sg/wind_hdf5_plt_cnt_0193-projection-z-1280x0256.npy"))
            data.append(np.load("done-L6-M00-d100-sg/wind_hdf5_plt_cnt_0193-projection-x-0256x0256.npy"))
            time1 = "19.3"
            time2 = "38.7"
            dens = "1.0"

        for i in np.arange(0,len(data),2):
            im0 = axes[i].imshow(np.log10(data[i]).T,     aspect=1.0, extent=extent1, vmin=lvmin, vmax=lvmax, cmap=cmap_cd)
        for i in np.arange(1,len(data),2):
            im0 = axes[i].imshow(np.log10(data[i]).T,     aspect=1.0, extent=extent2, vmin=lvmin, vmax=lvmax, cmap=cmap_cd)

        for i in range(2,12):
            axes[i].set_xticklabels([])
        for i in range(1,12,2):
            axes[i].set_yticklabels([])
        
        axes[ 0].set_xlabel("$x$ \\textbf{(pc)}")
        axes[ 1].set_xlabel("$z$ \\textbf{(pc)}")
        axes[ 0].set_ylabel("$y$ \\textbf{(pc)}")
        axes[ 2].set_ylabel("$y$ \\textbf{(pc)}")
        axes[ 4].set_ylabel("$y$ \\textbf{(pc)}")
        axes[ 6].set_ylabel("$y$ \\textbf{(pc)}")
        axes[ 8].set_ylabel("$y$ \\textbf{(pc)}")
        axes[10].set_ylabel("$y$ \\textbf{(pc)}")

        # add text for sim time and sim name
        for i in [0,4,8]:
            axes[2+i].text(-80,70, "$t=1.25\\,t_\\mathrm{cc}="+time1+"$ \\textbf{Myr}", color="white")
            axes[0+i].text(-80,70, "$t=2.5\\,t_\\mathrm{cc}="+time2+"$ \\textbf{Myr}", color="white")
        axes[10].text(-100,130, "$\\bm{B}=\\bm{0},~~n_\\mathrm{c}="+dens+"\\,\\mathrm{cm^{-3}},~~+$\\textbf{self-gravity}")
        axes[ 6].text(-100,130, "$\\bm{B}\\parallel\\bm{v}_\\mathrm{w},~~n_\\mathrm{c}="+dens+"\\,\\mathrm{cm^{-3}},~~+$\\textbf{self-gravity}")
        axes[ 2].text(-100,130, "$\\bm{B}\\perp\\bm{v}_\\mathrm{w},~~n_\\mathrm{c}="+dens+"\\,\\mathrm{cm^{-3}},~~+$\\textbf{self-gravity}")

        #axes[10].text(-120,120, "\\textbf{\\texttt{B0-d100-sg}}", color="white", bbox=dict(boxstyle="square", ec=(0, 0, 0), fc=(0, 0, 0)))
        #axes[6].text(-130,130, "\\textbf{\\texttt{Bx-d100-sg}}", color="white", bbox=dict(boxstyle="square", ec=(0, 0, 0), fc=(0, 0, 0)))
        #axes[2].text(-150,130, "\\textbf{\\texttt{By-d100-sg}}", color="white", bbox=dict(boxstyle="square", ec=(0, 0, 0), fc=(0, 0, 0)))

        # remove some ticks
        axes[ 0].set_yticks([-100,0])
        axes[ 4].set_yticks([-100,0])
        axes[ 8].set_yticks([-100,0])
        
        # add colorbar axes
        ypos = ypos + ypad_cbar
        axes.append(plt.axes([xpos, ypos, sizex_cbar_h, sizey_cbar_h]))
        
        cba = plt.colorbar(im0, cax=axes[len(data)], orientation="horizontal", ticks=range(int(lvmin), int(lvmax+1)))
                
        axes[len(data)].xaxis.set_label_position('top') 
        axes[len(data)].set_xlabel("\\textbf{log[ column density (g cm$^{-2}$) ]}")
        
        fig.savefig("plots/coldens-tcc/"+cloud+"-tcc-125-250.png", bbox_inches="tight")
        fig.savefig("plots/coldens-tcc/"+cloud+"-tcc-125-250.pdf", bbox_inches="tight")


if args.mode == "CD_warm_ion":
    print("plot coldens of warm mionized gas at around point of max mass")

    for cloud in [ "d010", "d050", "d100" ]:

        ds = yt.load("done-L6-M00-d010-sg/wind_hdf5_plt_cnt_0000")
        extent1 = [ds.domain_left_edge[0].in_units("pc"),ds.domain_right_edge[0].in_units("pc"),ds.domain_left_edge[1].in_units("pc"),ds.domain_right_edge[1].in_units("pc")]
        extent2 = [ds.domain_left_edge[1].in_units("pc"),ds.domain_right_edge[1].in_units("pc"),ds.domain_left_edge[2].in_units("pc"),ds.domain_right_edge[2].in_units("pc")]

        fig = plt.figure(figsize=(8,8))
        
        Nimg=6
        
        lmargin=0.1
        bmargin=0.1
        sizex=0.6
        sizey=sizex/5.
        xpad=0.01
        ypad=0.01
        ypad2=0.05
        
        sizex_cbar_h = sizex+xpad+sizey
        sizey_cbar_h = 0.02
        sizex_cbar_v = 0.02
        sizey_cbar_v = Nimg*sizey + (Nimg-1)*ypad
        xpad_cbar = 0.025
        ypad_cbar = 0.01
        
        axes = []
        xpos = lmargin
        ypos = bmargin
        for g in range(3):
            for i in range(2):
                print(xpos, ypos, sizex, sizey, xpos+sizex+xpad+sizey)
                axes.append(plt.axes([xpos,            ypos, sizex, sizey]))
                axes.append(plt.axes([xpos+sizex+xpad, ypos, sizey, sizey]))
                ypos = ypos + sizey + ypad
            ypos = ypos + ypad2

        # pictures arranges from bottom to top

        if cloud == "d010":
            data = []
            data.append(np.load("done-L6-M6y-d010-sg/wind_hdf5_plt_cnt_0150-projection-z-1280x0256-dens_warm_ion.npy"))
            data.append(np.load("done-L6-M6y-d010-sg/wind_hdf5_plt_cnt_0150-projection-x-0256x0256-dens_warm_ion.npy"))
            data.append(np.load("done-L6-M6y-d010-sg/wind_hdf5_plt_cnt_0150-projection-z-1280x0256.npy"))
            data.append(np.load("done-L6-M6y-d010-sg/wind_hdf5_plt_cnt_0150-projection-x-0256x0256.npy"))
            data.append(np.load("done-L6-M6x-d010-sg/wind_hdf5_plt_cnt_0200-projection-z-1280x0256-dens_warm_ion.npy"))
            data.append(np.load("done-L6-M6x-d010-sg/wind_hdf5_plt_cnt_0200-projection-x-0256x0256-dens_warm_ion.npy"))
            data.append(np.load("done-L6-M6x-d010-sg/wind_hdf5_plt_cnt_0200-projection-z-1280x0256.npy"))
            data.append(np.load("done-L6-M6x-d010-sg/wind_hdf5_plt_cnt_0200-projection-x-0256x0256.npy"))
            data.append(np.load("done-L6-M00-d010-sg/wind_hdf5_plt_cnt_0200-projection-z-1280x0256-dens_warm_ion.npy"))
            data.append(np.load("done-L6-M00-d010-sg/wind_hdf5_plt_cnt_0200-projection-x-0256x0256-dens_warm_ion.npy"))
            data.append(np.load("done-L6-M00-d010-sg/wind_hdf5_plt_cnt_0200-projection-z-1280x0256.npy"))
            data.append(np.load("done-L6-M00-d010-sg/wind_hdf5_plt_cnt_0200-projection-x-0256x0256.npy"))
            time1 = "15"
            time2 = "20"
            dens = "0.1"

        if cloud == "d050":
            data = []
            data.append(np.load(     "L6-M6y-d050-sg/wind_hdf5_plt_cnt_0300-projection-z-1280x0256-dens_warm_ion.npy"))
            data.append(np.load(     "L6-M6y-d050-sg/wind_hdf5_plt_cnt_0300-projection-x-0256x0256-dens_warm_ion.npy"))
            data.append(np.load(     "L6-M6y-d050-sg/wind_hdf5_plt_cnt_0300-projection-z-1280x0256.npy"))
            data.append(np.load(     "L6-M6y-d050-sg/wind_hdf5_plt_cnt_0300-projection-x-0256x0256.npy"))
            data.append(np.load("done-L6-M6x-d050-sg/wind_hdf5_plt_cnt_0300-projection-z-1280x0256-dens_warm_ion.npy"))
            data.append(np.load("done-L6-M6x-d050-sg/wind_hdf5_plt_cnt_0300-projection-x-0256x0256-dens_warm_ion.npy"))
            data.append(np.load("done-L6-M6x-d050-sg/wind_hdf5_plt_cnt_0300-projection-z-1280x0256.npy"))
            data.append(np.load("done-L6-M6x-d050-sg/wind_hdf5_plt_cnt_0300-projection-x-0256x0256.npy"))
            data.append(np.load("done-L6-M00-d050-sg/wind_hdf5_plt_cnt_0300-projection-z-1280x0256-dens_warm_ion.npy"))
            data.append(np.load("done-L6-M00-d050-sg/wind_hdf5_plt_cnt_0300-projection-x-0256x0256-dens_warm_ion.npy"))
            data.append(np.load("done-L6-M00-d050-sg/wind_hdf5_plt_cnt_0300-projection-z-1280x0256.npy"))
            data.append(np.load("done-L6-M00-d050-sg/wind_hdf5_plt_cnt_0300-projection-x-0256x0256.npy"))
            time1 = "30"
            time2 = "30"
            dens = "0.5"

        if cloud == "d100":
            data = []
            data.append(np.load("done-L6-M6y-d100-sg/wind_hdf5_plt_cnt_0300-projection-z-1280x0256-dens_warm_ion.npy"))
            data.append(np.load("done-L6-M6y-d100-sg/wind_hdf5_plt_cnt_0300-projection-x-0256x0256-dens_warm_ion.npy"))
            data.append(np.load("done-L6-M6y-d100-sg/wind_hdf5_plt_cnt_0300-projection-z-1280x0256.npy"))
            data.append(np.load("done-L6-M6y-d100-sg/wind_hdf5_plt_cnt_0300-projection-x-0256x0256.npy"))
            data.append(np.load("done-L6-M6x-d100-sg/wind_hdf5_plt_cnt_0300-projection-z-1280x0256-dens_warm_ion.npy"))
            data.append(np.load("done-L6-M6x-d100-sg/wind_hdf5_plt_cnt_0300-projection-x-0256x0256-dens_warm_ion.npy"))
            data.append(np.load("done-L6-M6x-d100-sg/wind_hdf5_plt_cnt_0300-projection-z-1280x0256.npy"))
            data.append(np.load("done-L6-M6x-d100-sg/wind_hdf5_plt_cnt_0300-projection-x-0256x0256.npy"))
            data.append(np.load("done-L6-M00-d100-sg/wind_hdf5_plt_cnt_0300-projection-z-1280x0256-dens_warm_ion.npy"))
            data.append(np.load("done-L6-M00-d100-sg/wind_hdf5_plt_cnt_0300-projection-x-0256x0256-dens_warm_ion.npy"))
            data.append(np.load("done-L6-M00-d100-sg/wind_hdf5_plt_cnt_0300-projection-z-1280x0256.npy"))
            data.append(np.load("done-L6-M00-d100-sg/wind_hdf5_plt_cnt_0300-projection-x-0256x0256.npy"))
            time1 = "30"
            time2 = "30"
            dens = "1.0"

        for i in np.arange(0,len(data),2):
            im0 = axes[i].imshow(np.log10(data[i]).T,     aspect=1.0, extent=extent1, vmin=lvmin, vmax=lvmax, cmap=cmap_cd)
        for i in np.arange(1,len(data),2):
            im0 = axes[i].imshow(np.log10(data[i]).T,     aspect=1.0, extent=extent2, vmin=lvmin, vmax=lvmax, cmap=cmap_cd)

        for i in range(2,12):
            axes[i].set_xticklabels([])
        for i in range(1,12,2):
            axes[i].set_yticklabels([])
        
        axes[ 0].set_xlabel("$x$ \\textbf{(pc)}")
        axes[ 1].set_xlabel("$z$ \\textbf{(pc)}")
        axes[ 0].set_ylabel("$y$ \\textbf{(pc)}")
        axes[ 2].set_ylabel("$y$ \\textbf{(pc)}")
        axes[ 4].set_ylabel("$y$ \\textbf{(pc)}")
        axes[ 6].set_ylabel("$y$ \\textbf{(pc)}")
        axes[ 8].set_ylabel("$y$ \\textbf{(pc)}")
        axes[10].set_ylabel("$y$ \\textbf{(pc)}")

        # add text for sim time and sim name
        for i in [0,4,8]:
            axes[2+i].text(-80,70, "$t="+time1+"$ \\textbf{Myr,~~total column density}", color="black")
            axes[0+i].text(-80,70, "\\textbf{warm ionized medium}", color="black")
        axes[10].text(-100,130, "$\\bm{B}=\\bm{0},~~n_\\mathrm{c}="+dens+"\\,\\mathrm{cm^{-3}},~~+$\\textbf{self-gravity}")
        axes[ 6].text(-100,130, "$\\bm{B}\\parallel\\bm{v}_\\mathrm{w},~~n_\\mathrm{c}="+dens+"\\,\\mathrm{cm^{-3}},~~+$\\textbf{self-gravity}")
        axes[ 2].text(-100,130, "$\\bm{B}\\perp\\bm{v}_\\mathrm{w},~~n_\\mathrm{c}="+dens+"\\,\\mathrm{cm^{-3}},~~+$\\textbf{self-gravity}")

        #axes[10].text(-120,120, "\\textbf{\\texttt{B0-d100-sg}}", color="white", bbox=dict(boxstyle="square", ec=(0, 0, 0), fc=(0, 0, 0)))
        #axes[6].text(-130,130, "\\textbf{\\texttt{Bx-d100-sg}}", color="white", bbox=dict(boxstyle="square", ec=(0, 0, 0), fc=(0, 0, 0)))
        #axes[2].text(-150,130, "\\textbf{\\texttt{By-d100-sg}}", color="white", bbox=dict(boxstyle="square", ec=(0, 0, 0), fc=(0, 0, 0)))

        # remove some ticks
        axes[ 0].set_yticks([-100,0])
        axes[ 4].set_yticks([-100,0])
        axes[ 8].set_yticks([-100,0])
        
        # add colorbar axes
        ypos = ypos + ypad_cbar
        axes.append(plt.axes([xpos, ypos, sizex_cbar_h, sizey_cbar_h]))
        
        cba = plt.colorbar(im0, cax=axes[len(data)], orientation="horizontal", ticks=range(int(lvmin), int(lvmax+1)))
                
        axes[len(data)].xaxis.set_label_position('top') 
        axes[len(data)].set_xlabel("\\textbf{log[ column density (g cm$^{-2}$) ]}")
        
        fig.savefig("plots/coldens-WIM/"+cloud+".png", bbox_inches="tight")
        fig.savefig("plots/coldens-WIM/"+cloud+".pdf", bbox_inches="tight")



if args.mode == "tcc25chem":
    print("generate a plot 2.5 tcc with checmical composition for d100")

    for cloud in [ "d100" ]:

        ds = yt.load("done-L6-M00-d010-sg/wind_hdf5_plt_cnt_0000")
        extent1 = [ds.domain_left_edge[0].in_units("pc"),ds.domain_right_edge[0].in_units("pc"),ds.domain_left_edge[1].in_units("pc"),ds.domain_right_edge[1].in_units("pc")]
        extent2 = [ds.domain_left_edge[1].in_units("pc"),ds.domain_right_edge[1].in_units("pc"),ds.domain_left_edge[2].in_units("pc"),ds.domain_right_edge[2].in_units("pc")]

        fig = plt.figure(figsize=(8,8))
        
        Nimg=6
        
        lmargin=0.1
        bmargin=0.1
        sizex=0.6
        sizey=sizex/5.
        xpad=0.01
        ypad=0.01
        ypad2=0.05
        
        sizex_cbar_h = sizex+xpad+sizey
        sizey_cbar_h = 0.02
        sizex_cbar_v = 0.02
        sizey_cbar_v = Nimg*sizey + (Nimg-1)*ypad
        xpad_cbar = 0.025
        ypad_cbar = 0.01
        
        axes = []
        xpos = lmargin
        ypos = bmargin
        for g in range(3): # three groups
            for i in range(3): # three plots each
                print(xpos, ypos, sizex, sizey, xpos+sizex+xpad+sizey)
                axes.append(plt.axes([xpos,            ypos, sizex, sizey]))
                axes.append(plt.axes([xpos+sizex+xpad, ypos, sizey, sizey]))
                ypos = ypos + sizey + ypad
            ypos = ypos + ypad2

        # pictures arranges from bottom to top

        if cloud == "d010":
            data = []
            print("code not implemented, copy from d100 for files 0122! EXIT!")
            exit()
            time = "12.2"
            dens = "0.1"

        if cloud == "d050":
            data = []
            print("code not implemented, copy from d100 for files 0273! EXIT!")
            exit()
            time = "27.3"
            dens = "0.5"

        if cloud == "d100":
            data = []
            data.append(np.load("done-L6-M6y-d100-sg/wind_hdf5_plt_cnt_0387-projection-z-1280x0256-densh2.npy"))
            data.append(np.load("done-L6-M6y-d100-sg/wind_hdf5_plt_cnt_0387-projection-x-0256x0256-densh2.npy"))
            data.append(np.load("done-L6-M6y-d100-sg/wind_hdf5_plt_cnt_0387-projection-z-1280x0256-densha.npy"))
            data.append(np.load("done-L6-M6y-d100-sg/wind_hdf5_plt_cnt_0387-projection-x-0256x0256-densha.npy"))
            data.append(np.load("done-L6-M6y-d100-sg/wind_hdf5_plt_cnt_0387-projection-z-1280x0256-denshp.npy"))
            data.append(np.load("done-L6-M6y-d100-sg/wind_hdf5_plt_cnt_0387-projection-x-0256x0256-denshp.npy"))

            data.append(np.load("done-L6-M6x-d100-sg/wind_hdf5_plt_cnt_0387-projection-z-1280x0256-densh2.npy"))
            data.append(np.load("done-L6-M6x-d100-sg/wind_hdf5_plt_cnt_0387-projection-x-0256x0256-densh2.npy"))
            data.append(np.load("done-L6-M6x-d100-sg/wind_hdf5_plt_cnt_0387-projection-z-1280x0256-densha.npy"))
            data.append(np.load("done-L6-M6x-d100-sg/wind_hdf5_plt_cnt_0387-projection-x-0256x0256-densha.npy"))
            data.append(np.load("done-L6-M6x-d100-sg/wind_hdf5_plt_cnt_0387-projection-z-1280x0256-denshp.npy"))
            data.append(np.load("done-L6-M6x-d100-sg/wind_hdf5_plt_cnt_0387-projection-x-0256x0256-denshp.npy"))

            data.append(np.load("done-L6-M00-d100-sg/wind_hdf5_plt_cnt_0387-projection-z-1280x0256-densh2.npy"))
            data.append(np.load("done-L6-M00-d100-sg/wind_hdf5_plt_cnt_0387-projection-x-0256x0256-densh2.npy"))
            data.append(np.load("done-L6-M00-d100-sg/wind_hdf5_plt_cnt_0387-projection-z-1280x0256-densha.npy"))
            data.append(np.load("done-L6-M00-d100-sg/wind_hdf5_plt_cnt_0387-projection-x-0256x0256-densha.npy"))
            data.append(np.load("done-L6-M00-d100-sg/wind_hdf5_plt_cnt_0387-projection-z-1280x0256-denshp.npy"))
            data.append(np.load("done-L6-M00-d100-sg/wind_hdf5_plt_cnt_0387-projection-x-0256x0256-denshp.npy"))

            time = "38.7"
            dens = "1.0"

        for i in np.arange(0,len(data),2):
            im0 = axes[i].imshow(np.log10(data[i]).T,     aspect=1.0, extent=extent1, vmin=lvmin, vmax=lvmax, cmap=cmap_cd)
        for i in np.arange(1,len(data),2):
            im0 = axes[i].imshow(np.log10(data[i]).T,     aspect=1.0, extent=extent2, vmin=lvmin, vmax=lvmax, cmap=cmap_cd)

        for i in range(2,18):
            axes[i].set_xticklabels([])
        for i in range(1,18,2):
            axes[i].set_yticklabels([])
        
        axes[ 0].set_xlabel("$x$ \\textbf{(pc)}")
        axes[ 1].set_xlabel("$z$ \\textbf{(pc)}")
        axes[ 0].set_ylabel("$y$ \\textbf{(pc)}")
        axes[ 2].set_ylabel("$y$ \\textbf{(pc)}")
        axes[ 4].set_ylabel("$y$ \\textbf{(pc)}")
        axes[ 6].set_ylabel("$y$ \\textbf{(pc)}")
        axes[ 8].set_ylabel("$y$ \\textbf{(pc)}")
        axes[10].set_ylabel("$y$ \\textbf{(pc)}")
        axes[12].set_ylabel("$y$ \\textbf{(pc)}")
        axes[14].set_ylabel("$y$ \\textbf{(pc)}")
        axes[16].set_ylabel("$y$ \\textbf{(pc)}")

        # add text for sim time and sim name, here the panels are hard-coded, YES!!!!!
        for i in [0,6,12]:
            #axes[2+i].text(-80,70, "$t=1.25\\,t_\\mathrm{cc}="+time+"$ \\textbf{Myr}", color="white")
            axes[4+i].text(-80,70, "\\textbf{ionized}", color="white")
            axes[2+i].text(-80,70, "\\textbf{atomic}", color="white")
            axes[0+i].text(-80,70, "\\textbf{molecular}", color="white")
        axes[16].text(-100,130, "$\\bm{B}=\\bm{0},~~n_\\mathrm{c}="+dens+"\\,\\mathrm{cm^{-3}},~~+$\\textbf{self-gravity},~~$t=2.5\\,t_\\mathrm{cc}="+time+"$ \\textbf{Myr}")
        axes[10].text(-100,130, "$\\bm{B}\\parallel\\bm{v}_\\mathrm{w},~~n_\\mathrm{c}="+dens+"\\,\\mathrm{cm^{-3}},~~+$\\textbf{self-gravity},~~$t=2.5\\,t_\\mathrm{cc}="+time+"$ \\textbf{Myr}")
        axes[ 4].text(-100,130, "$\\bm{B}\\perp\\bm{v}_\\mathrm{w},~~n_\\mathrm{c}="+dens+"\\,\\mathrm{cm^{-3}},~~+$\\textbf{self-gravity},~~$t=2.5\\,t_\\mathrm{cc}="+time+"$ \\textbf{Myr}")

        #axes[10].text(-120,120, "\\textbf{\\texttt{B0-d100-sg}}", color="white", bbox=dict(boxstyle="square", ec=(0, 0, 0), fc=(0, 0, 0)))
        #axes[6].text(-130,130, "\\textbf{\\texttt{Bx-d100-sg}}", color="white", bbox=dict(boxstyle="square", ec=(0, 0, 0), fc=(0, 0, 0)))
        #axes[2].text(-150,130, "\\textbf{\\texttt{By-d100-sg}}", color="white", bbox=dict(boxstyle="square", ec=(0, 0, 0), fc=(0, 0, 0)))

        # remove some ticks
        axes[ 0].set_yticks([-100,0])
        axes[ 2].set_yticks([-100,0])
        axes[ 0].set_yticks([-100,0])
        axes[ 6].set_yticks([-100,0])
        axes[ 8].set_yticks([-100,0])
        axes[12].set_yticks([-100,0])
        axes[14].set_yticks([-100,0])
        
        # add colorbar axes
        ypos = ypos + ypad_cbar
        axes.append(plt.axes([xpos, ypos, sizex_cbar_h, sizey_cbar_h]))
        
        cba = plt.colorbar(im0, cax=axes[len(data)], orientation="horizontal", ticks=range(int(lvmin), int(lvmax+1)))
                
        axes[len(data)].xaxis.set_label_position('top') 
        axes[len(data)].set_xlabel("\\textbf{log[ column density (g cm$^{-2}$) ]}")
        
        fig.savefig("plots/coldens-chem-tcc/"+cloud+"-tcc-250.png", bbox_inches="tight")
        fig.savefig("plots/coldens-chem-tcc/"+cloud+"-tcc-250.pdf", bbox_inches="tight")



if args.mode == "CD_3_comp":
    print("plot column densities for three different fields (winds, dens)")

    for pltcnt in args.plt:
        pltfmt=pltcnt.zfill(4)

        ds = yt.load("done-L6-M00-d010-ng/wind_hdf5_plt_cnt_"+pltfmt)
        extent1 = [ds.domain_left_edge[0].in_units("pc"),ds.domain_right_edge[0].in_units("pc"),ds.domain_left_edge[1].in_units("pc"),ds.domain_right_edge[1].in_units("pc")]
        extent2 = [ds.domain_left_edge[1].in_units("pc"),ds.domain_right_edge[1].in_units("pc"),ds.domain_left_edge[2].in_units("pc"),ds.domain_right_edge[2].in_units("pc")]
        
        fig = plt.figure(figsize=(8,8))
            
        Nimg=6
        
        lmargin=0.1
        bmargin=0.1
        sizex=0.6
        sizey=sizex/5.
        xpad=0.01
        ypad=0.01
        ypad2=0.05
            
        sizex_cbar_h = sizex+xpad+sizey
        sizey_cbar_h = 0.02
        sizex_cbar_v = 0.02
        sizey_cbar_v = Nimg*sizey + (Nimg-1)*ypad
        xpad_cbar = 0.025
        ypad_cbar = 0.01
            
        axes = []
        xpos = lmargin
        ypos = bmargin
        for i in range(3):
            print(xpos, ypos, sizex, sizey, xpos+sizex+xpad+sizey)
            axes.append(plt.axes([xpos,            ypos, sizex, sizey]))
            axes.append(plt.axes([xpos+sizex+xpad, ypos, sizey, sizey]))
            ypos = ypos + sizey + ypad
        ypos = ypos + ypad2

        # pictures arranges from bottom to top
        print(args.sim)
        if args.sim[0] == "d010-ng":
            data = []
            sims = ["done-L6-M00-d010-ng", "done-L6-M6x-d010-ng", "done-L6-M6y-d010-ng" ]
            dens = "0.1"
            text1= "$\\bm{B}=\\bm{0}$"
            text2= "$\\bm{B}\\parallel\\bm{v}_\\mathrm{w}$"
            text3= "$\\bm{B}\\perp\\bm{v}_\\mathrm{w}$"
            texta= "$n_\\mathrm{c}="+dens+"\\,\\mathrm{cm^{-3}},~~-$\\textbf{self-gravity}"
            ofile= "d010-ng-"+pltfmt
            
        if args.sim[0] == "d010-sg":
            data = []
            sims = ["done-L6-M00-d010-sg", "done-L6-M6x-d010-sg", "done-L6-M6y-d010-sg" ]
            dens = "0.1"
            text1= "$\\bm{B}=\\bm{0}$"
            text2= "$\\bm{B}\\parallel\\bm{v}_\\mathrm{w}$"
            text3= "$\\bm{B}\\perp\\bm{v}_\\mathrm{w}$"
            texta= "$n_\\mathrm{c}="+dens+"\\,\\mathrm{cm^{-3}},~~+$\\textbf{self-gravity}"
            ofile= "d010-sg-"+pltfmt

        if args.sim[0] == "d050-ng":
            data = []
            sims = ["done-L6-M00-d050-ng", "done-L6-M6x-d050-ng", "done-L6-M6y-d050-ng" ]
            dens = "0.5"
            text1= "$\\bm{B}=\\bm{0}$"
            text2= "$\\bm{B}\\parallel\\bm{v}_\\mathrm{w}$"
            text3= "$\\bm{B}\\perp\\bm{v}_\\mathrm{w}$"
            texta= "$n_\\mathrm{c}="+dens+"\\,\\mathrm{cm^{-3}},~~-$\\textbf{self-gravity}"
            ofile= "d050-ng-"+pltfmt

        if args.sim[0] == "d050-sg":
            data = []
            sims = ["done-L6-M00-d050-sg", "done-L6-M6x-d050-sg", "done-L6-M6y-d050-sg" ]
            dens = "0.5"
            text1= "$\\bm{B}=\\bm{0}$"
            text2= "$\\bm{B}\\parallel\\bm{v}_\\mathrm{w}$"
            text3= "$\\bm{B}\\perp\\bm{v}_\\mathrm{w}$"
            texta= "$n_\\mathrm{c}="+dens+"\\,\\mathrm{cm^{-3}},~~+$\\textbf{self-gravity}"
            ofile= "d050-sg-"+pltfmt

        if args.sim[0] == "d100-ng":
            data = []
            sims = ["done-L6-M00-d100-ng", "done-L6-M6x-d100-ng", "L6-M6y-d100-ng" ]
            dens = "1.0"
            text1= "$\\bm{B}=\\bm{0}$"
            text2= "$\\bm{B}\\parallel\\bm{v}_\\mathrm{w}$"
            text3= "$\\bm{B}\\perp\\bm{v}_\\mathrm{w}$"
            texta= "$n_\\mathrm{c}="+dens+"\\,\\mathrm{cm^{-3}},~~-$\\textbf{self-gravity}"
            ofile= "d100-ng-"+pltfmt

        if args.sim[0] == "d100-sg":
            data = []
            sims = ["done-L6-M00-d100-sg", "done-L6-M6x-d100-sg", "done-L6-M6y-d100-sg" ]
            dens = "1.0"
            text1= "$\\bm{B}=\\bm{0}$"
            text2= "$\\bm{B}\\parallel\\bm{v}_\\mathrm{w}$"
            text3= "$\\bm{B}\\perp\\bm{v}_\\mathrm{w}$"
            texta= "$n_\\mathrm{c}="+dens+"\\,\\mathrm{cm^{-3}},~~+$\\textbf{self-gravity}"
            ofile= "d100-sg-"+pltfmt

        print(sims)
        if os.path.isfile(sims[2]+"/wind_hdf5_plt_cnt_"+pltfmt+"-projection-z-1280x0256.npy"):
            data.append(np.load(sims[2]+"/wind_hdf5_plt_cnt_"+pltfmt+"-projection-z-1280x0256.npy"))
        if os.path.isfile(sims[2]+"/wind_hdf5_plt_cnt_"+pltfmt+"-projection-x-0256x0256.npy"):
            data.append(np.load(sims[2]+"/wind_hdf5_plt_cnt_"+pltfmt+"-projection-x-0256x0256.npy"))
        if os.path.isfile(sims[1]+"/wind_hdf5_plt_cnt_"+pltfmt+"-projection-z-1280x0256.npy"):
            data.append(np.load(sims[1]+"/wind_hdf5_plt_cnt_"+pltfmt+"-projection-z-1280x0256.npy"))
        if os.path.isfile(sims[1]+"/wind_hdf5_plt_cnt_"+pltfmt+"-projection-x-0256x0256.npy"):
            data.append(np.load(sims[1]+"/wind_hdf5_plt_cnt_"+pltfmt+"-projection-x-0256x0256.npy"))
        if os.path.isfile(sims[0]+"/wind_hdf5_plt_cnt_"+pltfmt+"-projection-z-1280x0256.npy"):
            data.append(np.load(sims[0]+"/wind_hdf5_plt_cnt_"+pltfmt+"-projection-z-1280x0256.npy"))
        if os.path.isfile(sims[0]+"/wind_hdf5_plt_cnt_"+pltfmt+"-projection-x-0256x0256.npy"):
            data.append(np.load(sims[0]+"/wind_hdf5_plt_cnt_"+pltfmt+"-projection-x-0256x0256.npy"))

        if len(data) > 5:
            time = str(np.round(ds.current_time.in_units("Myr").v,1))

            for i in np.arange(0,len(data),2):
                im0 = axes[i].imshow(np.log10(data[i]).T,     aspect=1.0, extent=extent1, vmin=lvmin, vmax=lvmax, cmap=cmap_cd)
            for i in np.arange(1,len(data),2):
                im0 = axes[i].imshow(np.log10(data[i]).T,     aspect=1.0, extent=extent2, vmin=lvmin, vmax=lvmax, cmap=cmap_cd)

            for i in range(2,6):
                axes[i].set_xticklabels([])
            for i in range(1,6,2):
                axes[i].set_yticklabels([])
        
            axes[ 0].set_xlabel("$x$ \\textbf{(pc)}")
            axes[ 1].set_xlabel("$z$ \\textbf{(pc)}")
            axes[ 0].set_ylabel("$y$ \\textbf{(pc)}")
            axes[ 2].set_ylabel("$y$ \\textbf{(pc)}")
            axes[ 4].set_ylabel("$y$ \\textbf{(pc)}")

            # add text for sim time and sim name, here the panels are hard-coded, YES!!!!!
            axes[4].text(-80,70, text1, color="white")
            axes[2].text(-80,70, text2, color="white")
            axes[0].text(-80,70, text3, color="white")
            axes[ 4].text(-100,130, texta+",~~$t="+time+"$ \\textbf{Myr}")

            # remove some ticks
            axes[ 0].set_yticks([-100,0])
            axes[ 2].set_yticks([-100,0])
            axes[ 0].set_yticks([-100,0])
            
            # add colorbar axes
            ypos = ypos + ypad_cbar
            axes.append(plt.axes([xpos, ypos, sizex_cbar_h, sizey_cbar_h]))
            
            cba = plt.colorbar(im0, cax=axes[len(data)], orientation="horizontal", ticks=range(int(lvmin), int(lvmax+1)))
            
            axes[len(data)].xaxis.set_label_position('top') 
            axes[len(data)].set_xlabel("\\textbf{log[ column density (g cm$^{-2}$) ]}")
            
            fig.savefig("plots/coldens-wind-comp/"+ofile+".png", bbox_inches="tight")
            fig.savefig("plots/coldens-wind-comp/"+ofile+".pdf", bbox_inches="tight")


if args.mode == "CD_chem_comp":
    print("plot column densities for three different chemical fields")

    for pltcnt in args.plt:
        pltfmt=pltcnt.zfill(4)

        ds = yt.load("done-L6-M00-d010-ng/wind_hdf5_plt_cnt_"+pltfmt)
        extent1 = [ds.domain_left_edge[0].in_units("pc"),ds.domain_right_edge[0].in_units("pc"),ds.domain_left_edge[1].in_units("pc"),ds.domain_right_edge[1].in_units("pc")]
        extent2 = [ds.domain_left_edge[1].in_units("pc"),ds.domain_right_edge[1].in_units("pc"),ds.domain_left_edge[2].in_units("pc"),ds.domain_right_edge[2].in_units("pc")]
        
        fig = plt.figure(figsize=(8,8))
            
        Nimg=6
        
        lmargin=0.1
        bmargin=0.1
        sizex=0.6
        sizey=sizex/5.
        xpad=0.01
        ypad=0.01
        ypad2=0.05
            
        sizex_cbar_h = sizex+xpad+sizey
        sizey_cbar_h = 0.02
        sizex_cbar_v = 0.02
        sizey_cbar_v = Nimg*sizey + (Nimg-1)*ypad
        xpad_cbar = 0.025
        ypad_cbar = 0.01
            
        axes = []
        xpos = lmargin
        ypos = bmargin
        for i in range(3):
            print(xpos, ypos, sizex, sizey, xpos+sizex+xpad+sizey)
            axes.append(plt.axes([xpos,            ypos, sizex, sizey]))
            axes.append(plt.axes([xpos+sizex+xpad, ypos, sizey, sizey]))
            ypos = ypos + sizey + ypad
        ypos = ypos + ypad2

        # pictures arranges from bottom to top
        print(args.sim)

        if "-d010-" in args.sim[0]:
            dens = "0.1"
        if "-d050-" in args.sim[0]:
            dens = "0.5"
        if "-d100-" in args.sim[0]:
            dens = "1"
        if "-ng" in args.sim[0]:
            grav = "$-$\\textbf{self-gravity}"
        if "-sg" in args.sim[0]:
            grav = "$+$\\textbf{self-gravity}"
        if "-M00-" in args.sim[0]:
            magn = "$\\bm{B}=\\bm{0}$"
        if "-M6x-" in args.sim[0]:
            magn = "$\\bm{B}\\parallel\\bm{v}_\\mathrm{w}$"
        if "-M6y-" in args.sim[0]:
            magn = "$\\bm{B}\\perp\\bm{v}_\\mathrm{w}$"

        texta= magn+",~~$n_\\mathrm{c}="+dens+"\\,\\mathrm{cm^{-3}},~~$"+grav
        ofile= args.sim[0]+"-"+pltfmt

        data = []
        
        # include clipping to min/max
        if os.path.isfile(args.sim[0]+"/wind_hdf5_plt_cnt_"+pltfmt+"-projection-z-1280x0256-densh2.npy"):
            data.append(np.clip(np.load(args.sim[0]+"/wind_hdf5_plt_cnt_"+pltfmt+"-projection-z-1280x0256-densh2.npy"), np.power(10.0,lvmin), np.power(10.0,lvmax)))
        if os.path.isfile(args.sim[0]+"/wind_hdf5_plt_cnt_"+pltfmt+"-projection-x-0256x0256-densh2.npy"):
            data.append(np.clip(np.load(args.sim[0]+"/wind_hdf5_plt_cnt_"+pltfmt+"-projection-x-0256x0256-densh2.npy"), np.power(10.0,lvmin), np.power(10.0,lvmax)))
        if os.path.isfile(args.sim[0]+"/wind_hdf5_plt_cnt_"+pltfmt+"-projection-z-1280x0256-densha.npy"):
            data.append(np.clip(np.load(args.sim[0]+"/wind_hdf5_plt_cnt_"+pltfmt+"-projection-z-1280x0256-densha.npy"), np.power(10.0,lvmin), np.power(10.0,lvmax)))
        if os.path.isfile(args.sim[0]+"/wind_hdf5_plt_cnt_"+pltfmt+"-projection-x-0256x0256-densha.npy"):
            data.append(np.clip(np.load(args.sim[0]+"/wind_hdf5_plt_cnt_"+pltfmt+"-projection-x-0256x0256-densha.npy"), np.power(10.0,lvmin), np.power(10.0,lvmax)))
        if os.path.isfile(args.sim[0]+"/wind_hdf5_plt_cnt_"+pltfmt+"-projection-z-1280x0256-dens_warm_ion.npy"):
            data.append(np.clip(np.load(args.sim[0]+"/wind_hdf5_plt_cnt_"+pltfmt+"-projection-z-1280x0256-dens_warm_ion.npy"), np.power(10.0,lvmin), np.power(10.0,lvmax)))
        if os.path.isfile(args.sim[0]+"/wind_hdf5_plt_cnt_"+pltfmt+"-projection-x-0256x0256-dens_warm_ion.npy"):
            data.append(np.clip(np.load(args.sim[0]+"/wind_hdf5_plt_cnt_"+pltfmt+"-projection-x-0256x0256-dens_warm_ion.npy"), np.power(10.0,lvmin), np.power(10.0,lvmax)))

        if len(data) > 5:
            time = str(np.round(ds.current_time.in_units("Myr").v,1))

            for i in np.arange(0,len(data),2):
                im0 = axes[i].imshow(np.log10(data[i]).T,     aspect=1.0, extent=extent1, vmin=lvmin, vmax=lvmax, cmap=cmap_cd)
            for i in np.arange(1,len(data),2):
                im0 = axes[i].imshow(np.log10(data[i]).T,     aspect=1.0, extent=extent2, vmin=lvmin, vmax=lvmax, cmap=cmap_cd)

            for i in range(2,6):
                axes[i].set_xticklabels([])
            for i in range(1,6,2):
                axes[i].set_yticklabels([])
        
            axes[ 0].set_xlabel("$x$ \\textbf{(pc)}")
            axes[ 1].set_xlabel("$z$ \\textbf{(pc)}")
            axes[ 0].set_ylabel("$y$ \\textbf{(pc)}")
            axes[ 2].set_ylabel("$y$ \\textbf{(pc)}")
            axes[ 4].set_ylabel("$y$ \\textbf{(pc)}")

            # add text for sim time and sim name, here the panels are hard-coded, YES!!!!!
            axes[4].text(-80,70, "\\textbf{warm ionized}", color="white")
            axes[2].text(-80,70, "\\textbf{atomic}", color="white")
            axes[0].text(-80,70, "\\textbf{molecular}", color="white")
            axes[ 4].text(-100,130, texta+",~~$t="+time+"$ \\textbf{Myr}")

            # remove some ticks
            axes[ 0].set_yticks([-100,0])
            axes[ 2].set_yticks([-100,0])
            axes[ 0].set_yticks([-100,0])
            
            # add colorbar axes
            ypos = ypos + ypad_cbar
            axes.append(plt.axes([xpos, ypos, sizex_cbar_h, sizey_cbar_h]))
            
            cba = plt.colorbar(im0, cax=axes[len(data)], orientation="horizontal", ticks=range(int(lvmin), int(lvmax+1)))
            
            axes[len(data)].xaxis.set_label_position('top') 
            axes[len(data)].set_xlabel("\\textbf{log[ column density (g cm$^{-2}$) ]}")
            
            fig.savefig("plots/coldens-chem-comp/"+ofile+".png", bbox_inches="tight")
            fig.savefig("plots/coldens-chem-comp/"+ofile+".pdf", bbox_inches="tight")



if args.mode == "tcc25CO":
    print("generate a plot at approx 2.5 tcc with chemical composition for including CO (only d100)")
    
    useN = True

    for cloud in [ "d100" ]:

        ds = yt.load("done-L6-M00-d100-sg/wind_hdf5_plt_cnt_0000")
        extent1 = [ds.domain_left_edge[0].in_units("pc"),ds.domain_right_edge[0].in_units("pc"),ds.domain_left_edge[1].in_units("pc"),ds.domain_right_edge[1].in_units("pc")]
        extent2 = [ds.domain_left_edge[1].in_units("pc"),ds.domain_right_edge[1].in_units("pc"),ds.domain_left_edge[2].in_units("pc"),ds.domain_right_edge[2].in_units("pc")]

        fig = plt.figure(figsize=(8,8))
        
        Nimg=6
        
        lmargin=0.1
        bmargin=0.1
        sizex=0.6
        sizey=sizex/5.
        xpad=0.01
        ypad=0.01
        ypad2=0.05
        
        sizex_cbar_h = sizex+xpad+sizey
        sizey_cbar_h = 0.02
        sizex_cbar_v = 0.02
        sizey_cbar_v = Nimg*sizey + (Nimg-1)*ypad
        xpad_cbar = 0.025
        ypad_cbar = 0.01
        
        axes = []
        xpos = lmargin
        ypos = bmargin
        for g in range(3): # three groups
            for i in range(3): # three plots each
                print(xpos, ypos, sizex, sizey, xpos+sizex+xpad+sizey)
                axes.append(plt.axes([xpos,            ypos, sizex, sizey]))
                axes.append(plt.axes([xpos+sizex+xpad, ypos, sizey, sizey]))
                ypos = ypos + sizey + ypad
            ypos = ypos + ypad2

        # pictures arranges from bottom to top

        if cloud == "d010":
            data = []
            print("code not implemented, copy from d100 for files 0122! EXIT!")
            exit()
            time = "12.2"
            dens = "0.1"

        if cloud == "d050":
            data = []
            print("code not implemented, copy from d100 for files 0273! EXIT!")
            exit()
            time = "27.3"
            dens = "0.5"

        if cloud == "d100":
            data = []
            data.append(np.load("done-L6-M6y-d100-sg/wind_hdf5_chk_0175-projection-z-1280x0256-densco.npy")*1e4)
            data.append(np.load("done-L6-M6y-d100-sg/wind_hdf5_chk_0175-projection-x-0256x0256-densco.npy")*1e4)
            data.append(np.load("done-L6-M6y-d100-sg/wind_hdf5_chk_0175-projection-z-1280x0256-densh2.npy"))
            data.append(np.load("done-L6-M6y-d100-sg/wind_hdf5_chk_0175-projection-x-0256x0256-densh2.npy"))
            data.append(np.load("done-L6-M6y-d100-sg/wind_hdf5_chk_0175-projection-z-1280x0256-densha.npy"))
            data.append(np.load("done-L6-M6y-d100-sg/wind_hdf5_chk_0175-projection-x-0256x0256-densha.npy"))

            data.append(np.load("done-L6-M6x-d100-sg/wind_hdf5_chk_0140-projection-z-1280x0256-densco.npy")*1e4)
            data.append(np.load("done-L6-M6x-d100-sg/wind_hdf5_chk_0140-projection-x-0256x0256-densco.npy")*1e4)
            data.append(np.load("done-L6-M6x-d100-sg/wind_hdf5_chk_0140-projection-z-1280x0256-densh2.npy"))
            data.append(np.load("done-L6-M6x-d100-sg/wind_hdf5_chk_0140-projection-x-0256x0256-densh2.npy"))
            data.append(np.load("done-L6-M6x-d100-sg/wind_hdf5_chk_0140-projection-z-1280x0256-densha.npy"))
            data.append(np.load("done-L6-M6x-d100-sg/wind_hdf5_chk_0140-projection-x-0256x0256-densha.npy"))

            data.append(np.load("done-L6-M00-d100-sg/wind_hdf5_chk_0135-projection-z-1280x0256-densco.npy")*1e4)
            data.append(np.load("done-L6-M00-d100-sg/wind_hdf5_chk_0135-projection-x-0256x0256-densco.npy")*1e4)
            data.append(np.load("done-L6-M00-d100-sg/wind_hdf5_chk_0135-projection-z-1280x0256-densh2.npy"))
            data.append(np.load("done-L6-M00-d100-sg/wind_hdf5_chk_0135-projection-x-0256x0256-densh2.npy"))
            data.append(np.load("done-L6-M00-d100-sg/wind_hdf5_chk_0135-projection-z-1280x0256-densha.npy"))
            data.append(np.load("done-L6-M00-d100-sg/wind_hdf5_chk_0135-projection-x-0256x0256-densha.npy"))

            time = "38.7"
            dens = "1.0"

            if useN:
                for i in range(len(data)):
                    data[i] = data[i]*Sigma_to_N

        if useN:
            fac=np.log10(Sigma_to_N)
        else:
            fac=0.0
        for i in np.arange(0,len(data),2):
            im0 = axes[i].imshow(np.log10(data[i]).T,     aspect=1.0, extent=extent1, vmin=lvmin+fac, vmax=lvmax+fac, cmap=cmap_cd)
        for i in np.arange(1,len(data),2):
            im0 = axes[i].imshow(np.log10(data[i]).T,     aspect=1.0, extent=extent2, vmin=lvmin+fac, vmax=lvmax+fac, cmap=cmap_cd)

        for i in range(2,18):
            axes[i].set_xticklabels([])
        for i in range(1,18,2):
            axes[i].set_yticklabels([])
        
        axes[ 0].set_xlabel("$x$ \\textbf{(pc)}")
        axes[ 1].set_xlabel("$z$ \\textbf{(pc)}")
        axes[ 0].set_ylabel("$y$ \\textbf{(pc)}")
        axes[ 2].set_ylabel("$y$ \\textbf{(pc)}")
        axes[ 4].set_ylabel("$y$ \\textbf{(pc)}")
        axes[ 6].set_ylabel("$y$ \\textbf{(pc)}")
        axes[ 8].set_ylabel("$y$ \\textbf{(pc)}")
        axes[10].set_ylabel("$y$ \\textbf{(pc)}")
        axes[12].set_ylabel("$y$ \\textbf{(pc)}")
        axes[14].set_ylabel("$y$ \\textbf{(pc)}")
        axes[16].set_ylabel("$y$ \\textbf{(pc)}")

        # add text for sim time and sim name, here the panels are hard-coded, YES!!!!!
        for i in [0,6,12]:
            #axes[2+i].text(-80,70, "$t=1.25\\,t_\\mathrm{cc}="+time+"$ \\textbf{Myr}", color="white")
            axes[4+i].text(-80,70, "\\textbf{atomic}", color="white")
            axes[2+i].text(-80,70, "\\textbf{molecular}", color="white")
            axes[0+i].text(-80,70, "\\textbf{CO}$\\times10^4$", color="white")
        axes[16].text(-100,130, "$\\bm{B}=\\bm{0},~~n_\\mathrm{c}="+dens+"\\,\\mathrm{cm^{-3}},~~+$\\textbf{self-gravity},~~$t=2.5\\,t_\\mathrm{cc}="+time+"$ \\textbf{Myr}")
        axes[10].text(-100,130, "$\\bm{B}\\parallel\\bm{v}_\\mathrm{w},~~n_\\mathrm{c}="+dens+"\\,\\mathrm{cm^{-3}},~~+$\\textbf{self-gravity},~~$t=2.5\\,t_\\mathrm{cc}="+time+"$ \\textbf{Myr}")
        axes[ 4].text(-100,130, "$\\bm{B}\\perp\\bm{v}_\\mathrm{w},~~n_\\mathrm{c}="+dens+"\\,\\mathrm{cm^{-3}},~~+$\\textbf{self-gravity},~~$t=2.5\\,t_\\mathrm{cc}="+time+"$ \\textbf{Myr}")

        #axes[10].text(-120,120, "\\textbf{\\texttt{B0-d100-sg}}", color="white", bbox=dict(boxstyle="square", ec=(0, 0, 0), fc=(0, 0, 0)))
        #axes[6].text(-130,130, "\\textbf{\\texttt{Bx-d100-sg}}", color="white", bbox=dict(boxstyle="square", ec=(0, 0, 0), fc=(0, 0, 0)))
        #axes[2].text(-150,130, "\\textbf{\\texttt{By-d100-sg}}", color="white", bbox=dict(boxstyle="square", ec=(0, 0, 0), fc=(0, 0, 0)))

        # remove some ticks
        axes[ 0].set_yticks([-100,0])
        axes[ 2].set_yticks([-100,0])
        axes[ 0].set_yticks([-100,0])
        axes[ 6].set_yticks([-100,0])
        axes[ 8].set_yticks([-100,0])
        axes[12].set_yticks([-100,0])
        axes[14].set_yticks([-100,0])
        
        # add colorbar axes
        ypos = ypos + ypad_cbar
        axes.append(plt.axes([xpos, ypos, sizex_cbar_h, sizey_cbar_h]))
        
        cba = plt.colorbar(im0, cax=axes[len(data)], orientation="horizontal", ticks=range(int(lvmin+fac), int(lvmax+1+fac)))
                
        axes[len(data)].xaxis.set_label_position('top')
        if useN:
            axes[len(data)].set_xlabel("\\textbf{log[ column density (g cm$^{-2}$) ]}")
        else:
            axes[len(data)].set_xlabel("\\textbf{log[ column density (cm$^{-2}$) ]}")
        
        fig.savefig("plots/coldens-chem-tcc/"+cloud+"-tcc-250-CO.png", bbox_inches="tight")
        fig.savefig("plots/coldens-chem-tcc/"+cloud+"-tcc-250-CO.pdf", bbox_inches="tight")




if args.mode == "time":

    for sim in args.sim:

        print("rendering time evolution for simulation: ", sim)
        print("using time snapshots:", args.plt)

        # parameter
        Nsim=len(args.plt)
        
        ref_file = sim+"/wind_hdf5_plt_cnt_"+args.plt[0].zfill(4)
        print("loading reference file:", ref_file)
        ds = yt.load(ref_file)
        extent1 = [ds.domain_left_edge[0].in_units("pc"),ds.domain_right_edge[0].in_units("pc"),ds.domain_left_edge[1].in_units("pc"),ds.domain_right_edge[1].in_units("pc")]
        extent2 = [ds.domain_left_edge[1].in_units("pc"),ds.domain_right_edge[1].in_units("pc"),ds.domain_left_edge[2].in_units("pc"),ds.domain_right_edge[2].in_units("pc")]

        # simulation files
        yfiles = []
        xfiles = []
        times  = []
        ims    = []

        for pltcnt in args.plt:
            pltfmt=pltcnt.zfill(4)
            print("loading data:", sim+"/wind_hdf5_plt_cnt_"+pltfmt+"-projection-z-1280x0256.npy")
            print("loading data:", sim+"/wind_hdf5_plt_cnt_"+pltfmt+"-projection-x-1280x0256.npy")
            yfiles.append(sim+"/wind_hdf5_plt_cnt_"+pltfmt+"-projection-z-1280x0256.npy")
            xfiles.append(sim+"/wind_hdf5_plt_cnt_"+pltfmt+"-projection-x-0256x0256.npy")
            times.append(str(int(pltcnt)/10))

        fig = plt.figure(figsize=(10,10*Nsim))
        grid = ImageGrid(fig, 111, (Nsim, 2), axes_pad=0.15, aspect=True, share_all=False, cbar_location="top", cbar_mode="single", cbar_pad=0.3)

        # fill images
        for i in range(Nsim):
            ydata = np.log10(np.load(yfiles[i]))
            ims.append(grid[2*i].imshow(ydata.T, aspect=1.0, extent=extent1, vmin=lvmin, vmax=lvmax, cmap=cmap_cd))
            grid[2*i].text(-80,80, "$t="+times[i]+"$ \\textbf{Myr}", color="white")
            xdata = np.log10(np.load(xfiles[i]))
            ims.append(grid[2*i+1].imshow(xdata.T, aspect=1.0, extent=extent2, vmin=lvmin, vmax=lvmax, cmap=cmap_cd))

            grid[2*i].set_ylabel("$y~~(\\mathrm{pc})$")

        # add axes and colorbars
        grid[2*Nsim-2].set_xlabel("$x~~(\\mathrm{pc})$")
        grid[2*Nsim-1].set_xlabel("$z~~(\\mathrm{pc})$")
        cax = grid.cbar_axes[0]
        cbar = cax.colorbar(ims[-1], ticks=np.arange(lvmin, lvmax+1))
        cax.set_xlabel("\\textbf{log[ column density (g cm$^{-2}$) ]}")
        cax.xaxis.set_ticks_position('bottom')

        odir, oname = get_output_var("plots/coldens", "time-"+sim, args)
        save_picture(odir, oname, fig, args)
