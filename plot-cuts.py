import yt
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
import argparse

plt.rcParams.update({
    "text.usetex": True,
    "font.family": "serif",
    "text.latex.preamble" : r'\boldmath'
})

resx = 1280
resy =  256

temp_warm0 = 8000.0
temp_warm1 = 3e5

def _dens_Hp(field, data):
    return (data["ihp "]*data["dens"].v)
yt.add_field("dens_Hp", function = _dens_Hp, units = "", sampling_type="cell")

def _dens_Ha(field, data):
    return (data["iha "]*data["dens"].v)
yt.add_field("dens_Ha", function = _dens_Ha, units = "", sampling_type="cell")

def _dens_H2(field, data):
    return (data["ih2 "]*data["dens"].v)
yt.add_field("dens_H2", function = _dens_H2, units = "", sampling_type="cell")

def _dens_warm_ion(field, data):
        return (np.where((data['temperature'].v >= temp_warm0) \
                         & (data['temperature'].v <= temp_warm1) \
                         & (data["ihp "].v > 0.5), data['density'].v, 0.0))
yt.add_field('dens_warm_ion', function = _dens_warm_ion, units = "", sampling_type="cell")


fields = ["temperature","density", "dens_warm_ion", "dens_Ha", "dens_H2"]
fnames = ["$\\mathrm{temp.}$", "$\\rho_\\mathrm{tot}$", "$\\rho_\\mathrm{WIM}$", "$\\rho_\\mathrm{atom.}$", "$\\rho_\\mathrm{mol.}$"]
cmaps  = ["plasma", "RdYlBu_r", "RdYlBu_r","RdYlBu_r", "RdYlBu_r"]
labels = ["$\\log~T~\\mathrm{(K)}$", "$\\log\\rho~(\\mathrm{g~cm}^{-3}$)", "$\\log\\rho~(\\mathrm{g~cm}^{-3}$)", "$\\log\\rho~(\\mathrm{g~cm}^{-3}$)", "$\\log\\rho~(\\mathrm{g~cm}^{-3}$)"]
mins       = [1, -27, -27, -27, -27]
maxs       = [6, -22, -22, -22, -22]

figsize_x = 20.0
figsize_y =  2.0


parser = argparse.ArgumentParser(description='cmd line args')
parser.add_argument('files', nargs='+', help='files')
parser.add_argument('-sim', default="", help='simulation name', type=str)
args = parser.parse_args()


for file in args.files:
    ds = yt.load(file)
    plt_cnt = file[-4:]
    #print(plt_cnt)
    extent1 = [ds.domain_left_edge[0].in_units("pc"),\
               ds.domain_right_edge[0].in_units("pc"),\
               ds.domain_left_edge[1].in_units("pc"),\
               ds.domain_right_edge[1].in_units("pc")]

    slc = yt.SlicePlot(ds, "z", fields=fields)

    
    fig, axes = plt.subplots(figsize=(10,figsize_y*len(fields)), nrows=len(fields), sharex=True)

    t = np.round(ds.current_time.in_units("Myr"),1)
    axes[0].set_title("\\textbf{"+args.sim+"}~$t="+str(t)+"~\\mathrm{Myr}$")
    for f,i,c in zip(fields,range(len(fields)),cmaps):
        data = np.array(slc.data_source.to_frb(ds.domain_width[0], (resx, resy), height=ds.domain_width[1])[f].T)
        log_data = np.clip(np.log10(data), mins[i], maxs[i])
        im = axes[i].imshow(log_data.T, aspect=1.0, extent=extent1, cmap=c, vmin=mins[i], vmax=maxs[i])
        axes[i].set_ylabel("$y~\\mathrm{(pc)}$")
        axes[i].text(-90,75, fnames[i], bbox=dict(boxstyle="square",
                                                  ec=(1., 0.5, 0.5),
                                                  fc=(1., 0.8, 0.8),
                                                  ))
        divider = make_axes_locatable(axes[i])
        cax = divider.append_axes('right', size='5%', pad=0.05)
        cb = plt.colorbar(im, cax=cax, orientation='vertical')
        cb.set_label(labels[i])
    axes[len(fields)-1].set_xlabel("$x~\\mathrm{(pc)}$")

    fig.savefig("cuts/cuts-"+plt_cnt+".pdf", bbox_inches="tight")
    fig.savefig("cuts/cuts-"+plt_cnt+".png", bbox_inches="tight", dpi=150)
