# plot clouds without wind

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import ImageGrid
import numpy as np
import yt
from matplotlib.ticker import LogLocator
from matplotlib.colors import LogNorm
from matplotlib import rc
from matplotlib import rcParams
rc('text', usetex=True)
rcParams['text.latex.preamble'] = [r'\boldmath \usepackage{bm}']
rc('font', family='serif')

from mpl_toolkits.axes_grid1 import make_axes_locatable

import argparse
import os

Msol=1.989e33


def _Mhp(field, data):
        return (data['cell_mass'].v*data['ihp '].v)
yt.add_field('Mhp', function = _Mhp, units = "")
def _Mha(field, data):
        return (data['cell_mass'].v*data['iha '].v)
yt.add_field('Mha', function = _Mha, units = "")
def _Mh2(field, data):
        return (data['cell_mass'].v*data['ih2 '].v)
yt.add_field('Mh2', function = _Mh2, units = "")


resx = 256
resy = 256
resz = 256
cmap = "RdYlBu_r"

files = []
sims = [ "L6-noW-d010-ng", "L6-noW-d010-sg", "L6-noW-d050-ng", "L6-noW-d050-sg", "L6-noW-d100-ng", "L6-noW-d100-sg"]
plts = [ "wind_hdf5_plt_cnt_0000", "wind_hdf5_plt_cnt_0100", "wind_hdf5_plt_cnt_0200", "wind_hdf5_plt_cnt_0300", "wind_hdf5_plt_cnt_0400" ]

lvmin = -6
lvmax = -2

for s in sims:
    for p in plts:
        files.append(s+"/"+p)

fig = plt.figure(figsize=(10,10))

extent1 = [-100, 100, -100, 100]

lmargin=0.1
bmargin=0.1
sizex=0.145
sizey=sizex
xpad=0.005
ypad=0.005

sizex_cbar_h = sizex
sizey_cbar_h = 0.02
sizex_cbar_v = 0.02
sizey_cbar_v = 0.02
xpad_cbar = 0.025
ypad_cbar = 0.05

axes = []
# order of the axes left to right top to bottom
for s in range(len(sims)):
    for p in range(len(plts)):
        axes.append(plt.axes([lmargin + float(p)*(sizex+xpad), bmargin + (len(sims)-1-float(s))*(sizey+ypad), sizex, sizey]))

cnt=0
for file in files:
    if not os.path.isfile(file):
        data = np.ones((256,256))
    else:
        ds = yt.load(file)
        if not os.path.isfile(file+"-coldens.npy"):
            data = np.array(yt.ProjectionPlot(ds, 'z', "density", center=[0,0,0]).data_source.to_frb(ds.domain_width[1], (resy, resz), height=ds.domain_width[2])["density"])
            np.save(file+"-coldens.npy", data)
        else:
            data=np.load(file+"-coldens.npy")

        ad = ds.all_data()
        #totalMHp, totalMHa, totalMH2, totalM = ad.quantities.total_quantity(["Mhp", "Mha", "Mh2", "cell_mass"])
        totalMH2, totalM = ad.quantities.total_quantity(["Mh2", "cell_mass"])

    im0 = axes[cnt].imshow(np.log10(data),     aspect=1.0, extent=extent1, vmin=lvmin, vmax=lvmax, cmap=cmap)
    #axes[cnt].text(-80,-80, "$f_{\\mathrm{H}_2}="+str(np.round(totalMH2/totalM,2))+"$", color="white")
    axes[cnt].text(-90,-90, "$M_{\\mathrm{H}_2}="+str(int(np.round(totalMH2/Msol,0)))+"\\,\\mathrm{M}_\\odot$", color="white")
    cnt=cnt+1

# unset all labels for the inner panels
for s in range(len(sims)):
    for p in range(len(plts)):
        if p > 0:
            axes[s*len(plts)+p].set_yticklabels([])
        if s < len(sims)-1:
            axes[s*len(plts)+p].set_xticklabels([])

# set xlabels for the lower row
for i in range((len(sims)-1)*len(plts),len(sims)*len(plts)):
    axes[i].set_xlabel("$x~~(\\mathrm{pc})$")
    axes[i].set_xticks([-100, 0, 50])

# set ylabels 
for i in range(len(sims)):
    axes[i*len(plts)].set_ylabel("$y~~(\\mathrm{pc})$")
    axes[i*len(plts)].set_yticks([-100, -50, 0, 50])

# set simualtion names
axes[0*len(plts)].text(-90,70, "$n_\\mathrm{c}=0.1\\,\\mathrm{cm}^{-3},\\,-\\mathrm{sg}$", color="white")
axes[1*len(plts)].text(-90,70, "$n_\\mathrm{c}=0.1\\,\\mathrm{cm}^{-3},\\,+\\mathrm{sg}$", color="white")
axes[2*len(plts)].text(-90,70, "$n_\\mathrm{c}=0.5\\,\\mathrm{cm}^{-3},\\,-\\mathrm{sg}$", color="white")
axes[3*len(plts)].text(-90,70, "$n_\\mathrm{c}=0.5\\,\\mathrm{cm}^{-3},\\,+\\mathrm{sg}$", color="white")
axes[4*len(plts)].text(-90,70, "$n_\\mathrm{c}=1.0\\,\\mathrm{cm}^{-3},\\,-\\mathrm{sg}$", color="white")
axes[5*len(plts)].text(-90,70, "$n_\\mathrm{c}=1.0\\,\\mathrm{cm}^{-3},\\,+\\mathrm{sg}$", color="white")

# set time as title for the upper row
axes[0].set_title("$t=0\\,\\mathrm{Myr}$")
axes[1].set_title("$t=10\\,\\mathrm{Myr}$")
axes[2].set_title("$t=20\\,\\mathrm{Myr}$")
axes[3].set_title("$t=30\\,\\mathrm{Myr}$")
axes[4].set_title("$t=40\\,\\mathrm{Myr}$")

fig.savefig("plots/coldens-noW/coldens-time.png", bbox_inches="tight")
fig.savefig("plots/coldens-noW/coldens-time.pdf", bbox_inches="tight")
