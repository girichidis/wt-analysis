# plot density slices and streamlines

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import ImageGrid
import numpy as np
import yt
from matplotlib.ticker import LogLocator
from matplotlib.colors import LogNorm
from matplotlib import rc
from matplotlib import rcParams
rc('text', usetex=True)
rcParams['text.latex.preamble'] = [r'\usepackage{bm}\usepackage{lmodern}\boldmath']
rc('font', family='serif')

from mpl_toolkits.axes_grid1 import make_axes_locatable
from mpl_toolkits.axes_grid1.colorbar import colorbar

import argparse
import os

try_img_grid = False

cmap="bone"

pc = 3.085e18
resx = 1280
resy =  256
resz =  256

resxs = resx//2
resys = resy//2
reszs = resz//2

sim1="done-L6-M00-d100-sg"
sim2="done-L6-M6x-d100-sg"
sim3="done-L6-M6y-d100-sg"

for times in ['0200', '0400']:

    # load slices of total gas from all three d100 sims at the end
    f1=sim1+"/wind_hdf5_plt_cnt_"+times
    f2=sim2+"/wind_hdf5_plt_cnt_"+times
    f3=sim3+"/wind_hdf5_plt_cnt_"+times
    
    ds1 = yt.load(f1)
    ds2 = yt.load(f2)
    ds3 = yt.load(f3)
    
    if not os.path.isfile(sim3+"/magn-"+times+".npy"):
        dens1 = np.array(yt.SlicePlot(ds1, 'z', "density").data_source.to_frb(ds1.domain_width[0], (resx, resy), height=ds1.domain_width[1])["density"])
        dens2 = np.array(yt.SlicePlot(ds2, 'z', "density").data_source.to_frb(ds2.domain_width[0], (resx, resy), height=ds2.domain_width[1])["density"])
        dens3 = np.array(yt.SlicePlot(ds3, 'z', "density").data_source.to_frb(ds3.domain_width[0], (resx, resy), height=ds3.domain_width[1])["density"])
    
        # get B fields to compute streamlines
        magx2 = np.array(yt.SlicePlot(ds2, 'z', "magx").data_source.to_frb(ds2.domain_width[0], (resxs, resys), height=ds2.domain_width[1])["magx"])
        magy2 = np.array(yt.SlicePlot(ds2, 'z', "magy").data_source.to_frb(ds2.domain_width[0], (resxs, resys), height=ds2.domain_width[1])["magy"])
        magn2 = np.array(yt.SlicePlot(ds2, 'z', "magnetic_field_strength").data_source.to_frb(ds2.domain_width[0], (resxs, resys), height=ds2.domain_width[1])["magnetic_field_strength"])
        magx3 = np.array(yt.SlicePlot(ds3, 'z', "magx").data_source.to_frb(ds3.domain_width[0], (resxs, resys), height=ds3.domain_width[1])["magx"])
        magy3 = np.array(yt.SlicePlot(ds3, 'z', "magy").data_source.to_frb(ds3.domain_width[0], (resxs, resys), height=ds3.domain_width[1])["magy"])
        magn3 = np.array(yt.SlicePlot(ds3, 'z', "magnetic_field_strength").data_source.to_frb(ds3.domain_width[0], (resxs, resys), height=ds3.domain_width[1])["magnetic_field_strength"])
        
        np.save(sim1+"/dens-"+times+".npy", dens1)
        np.save(sim2+"/dens-"+times+".npy", dens2)
        np.save(sim3+"/dens-"+times+".npy", dens3)
        
        np.save(sim2+"/magx-"+times+".npy", magx2)
        np.save(sim2+"/magy-"+times+".npy", magy2)
        np.save(sim2+"/magn-"+times+".npy", magn2)

        np.save(sim3+"/magx-"+times+".npy", magx3)
        np.save(sim3+"/magy-"+times+".npy", magy3)
        np.save(sim3+"/magn-"+times+".npy", magn3)
        
    else:

        dens1=np.load(sim1+"/dens-"+times+".npy")
        dens2=np.load(sim2+"/dens-"+times+".npy")
        dens3=np.load(sim3+"/dens-"+times+".npy")
              
        magx2=np.load(sim2+"/magx-"+times+".npy")
        magy2=np.load(sim2+"/magy-"+times+".npy")
        magn2=np.load(sim2+"/magn-"+times+".npy")
              
        magx3=np.load(sim3+"/magx-"+times+".npy")
        magy3=np.load(sim3+"/magy-"+times+".npy")
        magn3=np.load(sim3+"/magn-"+times+".npy")
        

    extent1 = [ds1.domain_left_edge[0].in_units("pc"),ds1.domain_right_edge[0].in_units("pc"),ds1.domain_left_edge[1].in_units("pc"),ds1.domain_right_edge[1].in_units("pc")]
    lvmin = [-28, np.log10(0.5)]
    lvmax = [-22, np.log10(3.0)]
    
    Y, X = np.mgrid[extent1[2]:extent1[3]:resys*1j, extent1[0]:extent1[1]:resxs*1j]
    print("shape: magn", magx2.shape, magy2.shape, Y.shape, X.shape)
    
    if try_img_grid:

        fig = plt.figure(figsize=(8,8))
        axes = ImageGrid(fig, 111, (3, 1), axes_pad=0.15, aspect=True, share_all=False, cbar_location="top", cbar_mode="single", cbar_pad=0.3)
        
        im0 = axes[0].imshow(np.log10(dens1),     aspect=1.0, extent=extent1, vmin=lvmin[0], vmax=lvmax[0], cmap=cmap)
        im1 = axes[1].imshow(np.log10(dens2),     aspect=1.0, extent=extent1, vmin=lvmin[0], vmax=lvmax[0], cmap=cmap)
        im2 = axes[2].imshow(np.log10(dens3),     aspect=1.0, extent=extent1, vmin=lvmin[0], vmax=lvmax[0], cmap=cmap)
        
        
        strm2 = axes[1].streamplot(X, Y, magx2, magy2, color=np.log10(magn2), linewidth=2, cmap=plt.cm.autumn, density=[1.0, 0.2])
        strm3 = axes[2].streamplot(X, Y, magx3, magy3, color=np.log10(magn3), linewidth=2, cmap=plt.cm.autumn, density=[1.0, 0.2])
        #fig.colorbar(strm2.lines)
        
        axes[0].set_xlim(extent1[0],extent1[1])
        axes[1].set_xlim(extent1[0],extent1[1])
        axes[2].set_xlim(extent1[0],extent1[1])
        
        axes[0].set_ylim(extent1[2],extent1[3])
        axes[1].set_ylim(extent1[2],extent1[3])
        axes[2].set_ylim(extent1[2],extent1[3])
        
        cax = axes.cbar_axes[0]
        cbar = cax.colorbar(strm2.lines)#, ticks=np.arange(lvmin, lvmax+1))
        cax.set_xlabel("\\textbf{log[ density (g cm$^{-3}$) ]}")
        cax.xaxis.set_ticks_position('bottom')
        
        fig.savefig("plots/streamlines/d100-"+times+"-dens-magn-strm.png", bbox_inches="tight")
        fig.savefig("plots/streamlines/d100-"+times+"-dens-magn-strm.pdf", bbox_inches="tight")

    else:

        fig = plt.figure(figsize=(8,8))
        
        sdens=3.0
    
        Nimg=3
        
        lmargin=0.1
        bmargin=0.1
        sizex=0.7
        sizey=sizex/5.
        xpad=0.05
        ypad=0.025
        
        sizex_cbar_h = sizex
        sizey_cbar_h = 0.02
        sizex_cbar_v = 0.02
        sizey_cbar_v = Nimg*sizey + (Nimg-1)*ypad
        xpad_cbar = 0.025
        ypad_cbar = 0.05
        
        # create three axes
        axes = []
        xpos = lmargin
        ypos = bmargin
        for i in range(Nimg):
            axes.append(plt.axes([xpos, bmargin + float(i)*ypad + float(i)*sizey, sizex, sizey]))
            
        # dens1 = np.clip(dens1, np.power(10.0,lvmin[0]), np.power(10.0,lvmax[0]))
        # dens2 = np.clip(dens2, np.power(10.0,lvmin[0]), np.power(10.0,lvmax[0]))
        # dens3 = np.clip(dens3, np.power(10.0,lvmin[0]), np.power(10.0,lvmax[0]))

        im0 = axes[2].imshow(np.log10(dens1),     aspect=1.0, extent=extent1, vmin=lvmin[0], vmax=lvmax[0], cmap=cmap)
        im1 = axes[1].imshow(np.log10(dens2),     aspect=1.0, extent=extent1, vmin=lvmin[0], vmax=lvmax[0], cmap=cmap)
        im2 = axes[0].imshow(np.log10(dens3),     aspect=1.0, extent=extent1, vmin=lvmin[0], vmax=lvmax[0], cmap=cmap)
        
        lmagn2 = np.log10(np.clip(magn2*1e6, 0.1, 10))
        lmagn3 = np.log10(np.clip(magn3*1e6, 0.1, 10))
        
        strm2 = axes[1].streamplot(X, Y, magx2, magy2, color=lmagn2, linewidth=1, norm=matplotlib.colors.Normalize(vmin=lvmin[1], vmax=lvmax[1]), cmap=plt.cm.autumn, density=[1.0*sdens, 0.2*sdens], arrowstyle="-")
        strm3 = axes[0].streamplot(X, Y, magx3, magy3, color=lmagn3, linewidth=1, norm=matplotlib.colors.Normalize(vmin=lvmin[1], vmax=lvmax[1]), cmap=plt.cm.autumn, density=[1.0*sdens, 0.2*sdens], arrowstyle="-")
        #fig.colorbar(strm2.lines)
        
        axes[0].set_xlim(extent1[0],extent1[1])
        axes[1].set_xlim(extent1[0],extent1[1])
        axes[2].set_xlim(extent1[0],extent1[1])
        
        axes[0].set_ylim(extent1[2],extent1[3])
        axes[1].set_ylim(extent1[2],extent1[3])
        axes[2].set_ylim(extent1[2],extent1[3])
        
        axes[1].set_xticklabels([])
        axes[2].set_xticklabels([])
        
        axes[0].set_xlabel("$x$ \\textbf{(pc)}")
        axes[0].set_ylabel("$y$ \\textbf{(pc)}")
        axes[1].set_ylabel("$y$ \\textbf{(pc)}")
        axes[2].set_ylabel("$y$ \\textbf{(pc)}")

        # add text for sim time and sim name
        axes[2].text(-80,70, "$t="+times[1:3]+"$ \\textbf{Myr}", color="white")
        #axes[2].text(-80,70, "$t="+times[1:3]+"$ \\textbf{Myr}", color="white", bbox=dict(boxstyle="square", ec=(0, 0, 0), fc=(0, 0, 0)))
        axes[2].text(550,70, "$\\bm{B}=\\bm{0},~n_\\mathrm{c}=1\\,\\mathrm{cm^{-3}},~+$\\textbf{sg}", color="white", bbox=dict(boxstyle="square", ec=(0, 0, 0), fc=(0, 0, 0)))
        axes[1].text(550,70, "$\\bm{B}\\parallel\\bm{v}_\\mathrm{w},~n_\\mathrm{c}=1\\,\\mathrm{cm^{-3}},~+$\\textbf{sg}", color="white", bbox=dict(boxstyle="square", ec=(0, 0, 0), fc=(0, 0, 0)))
        axes[0].text(550,70, "$\\bm{B}\\perp\\bm{v}_\\mathrm{w},~n_\\mathrm{c}=1\\,\\mathrm{cm^{-3}},~+$\\textbf{sg}", color="white", bbox=dict(boxstyle="square", ec=(0, 0, 0), fc=(0, 0, 0)))
        #axes[2].text(700,70, "\\textbf{\\texttt{B0-d100-sg}}", color="white", bbox=dict(boxstyle="square", ec=(0, 0, 0), fc=(0, 0, 0)))
        #axes[1].text(700,70, "\\textbf{\\texttt{Bx-d100-sg}}", color="white", bbox=dict(boxstyle="square", ec=(0, 0, 0), fc=(0, 0, 0)))
        #axes[0].text(700,70, "\\textbf{\\texttt{By-d100-sg}}", color="white", bbox=dict(boxstyle="square", ec=(0, 0, 0), fc=(0, 0, 0)))
        
        # add colorbar axes
        ypos = bmargin + float(Nimg-1)*ypad + float(Nimg)*sizey + ypad_cbar
        axes.append(plt.axes([xpos, ypos, sizex_cbar_h, sizey_cbar_h]))
        xpos = xpos + sizex + xpad_cbar
        axes.append(plt.axes([xpos, bmargin, sizex_cbar_v, sizey_cbar_v]))
        
        cba = plt.colorbar(im0, cax=axes[Nimg], orientation="horizontal", ticks=range(int(lvmin[0]), int(lvmax[0]+1)))
        
        Bticks  = [0.5, 1.0, 2.0, 3.0]
        sBticks = ['$0.5$', '$1$', '$2$', '$3$']
        lBticks = np.log10(Bticks)
        
        cbb = plt.colorbar(strm2.lines, cax=axes[Nimg+1], orientation="vertical", ticks=lBticks, norm=matplotlib.colors.Normalize(vmin=lvmin[1], vmax=lvmax[1]))
        cbb.mappable.set_clim(lvmin[1], lvmax[1])
        cbb.ax.set_yticklabels(sBticks)
        
        axes[Nimg].xaxis.set_label_position('top') 
        axes[Nimg].set_xlabel("\\textbf{log[ density (g cm$^{-3}$) ]}")
        axes[Nimg+1].set_ylabel("\\textbf{magnetic field strength ($\\mu$G)}")
        
        fig.savefig("plots/streamlines/d100-"+times+"-dens-magn-strm.png", bbox_inches="tight")
        fig.savefig("plots/streamlines/d100-"+times+"-dens-magn-strm.pdf", bbox_inches="tight")

