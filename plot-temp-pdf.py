import yt
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
import argparse

plt.rcParams.update({
    "text.usetex": True,
    "font.family": "serif",
    "text.latex.preamble" : r'\boldmath'
})

parser = argparse.ArgumentParser(description='cmd line args')
parser.add_argument('files', nargs='+', help='files')
parser.add_argument('-sim', default="", help='simulation name', type=str)
args = parser.parse_args()


colors = plt.cm.plasma(np.linspace(0,1,len(args.files)))

fig, (ax, cbar_ax) = plt.subplots(ncols=2, gridspec_kw={'width_ratios': [15, 1]})

t0 = 0.0
t1 = 0.0
for file, ifile in zip(args.files, range(len(args.files))):
    ds = yt.load(file)

    if ifile == 0:
        t0 = ds.current_time.in_units("Myr")
    if ifile == len(args.files)-1:
        t1 = ds.current_time.in_units("Myr")
    ad = ds.all_data()
    Vtot = np.sum(ad["cell_volume"])
    Mtot = np.sum(ad["cell_mass"])
    ed = np.linspace(2, 5, 100)
    H,ed = np.histogram(np.log10(ad["temperature"]), bins=ed, weights=ad["cell_volume"]/Vtot)
    ax.semilogy(ed[1:], H, color=colors[ifile])
    #x = np.linspace(0,10,100)
    #ax.semilogy(x, (ifile+1)*x, color=colors[ifile])

fig.subplots_adjust(wspace=0.03)
norm = plt.Normalize(vmin=t0, vmax=t1)
cbb = mpl.colorbar.ColorbarBase(cbar_ax, cmap="plasma", norm=norm, orientation="vertical")

cbar_ax.set_ylabel("$t~\\mathrm{(Myr)}$")
ax.set_xlabel("$\\log~T~\\mathrm{(K)}$")
ax.set_ylabel("\\textbf{PDF}")
fig.savefig("test-pdf.pdf", bbox_inches="tight")
