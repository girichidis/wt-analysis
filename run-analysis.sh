#!/bin/bash

echo
echo "usage:  $0  opt  sim(s)"
echo "  CD         : column density"
echo "  CDall      : column density for all plot files"
echo "  CDchem     : coldens ihp iha ih2"
echo "  CDchemall  : coldens ihp iha ih2"
echo "  CDchemtcc25: coldens ihp iha ih2 at 2.5 tcc"
echo "  glob       : glob quantities about B, chem, vel  (run with multiple files in cmd line)"
echo "  warm_ion   : global masses including warm ionized gas"
echo "  cold_vol   : computes volume of cold (<300K) and H2 gas (cell_vol*ih2)"
echo "  CD_warm_ion: global masses including warm ionized gas"
echo "  prof       : compute profiles along the x-axis"
echo "  thrmass    : masses above and below threshold densities and temperatures"
echo "  vturb      : kinetic energies and velocity dispersions"
echo "  pdfchem    : pdfs with chemical distributions"
echo
echo "  sim(s)     : simulation folder names. if empty, all are scanned"
echo "               Do not use wildcards if folder are not found in CWD!"
echo

Nparallel=5

function wait() {
    while [ $(ps -u $USER | grep python | wc -l) -ge $Nparallel ]; do echo "sleep"; sleep 2; done
}

export PATH=/u/phigi/YT-2018-03-29/yt-conda/bin:$PATH



if [ "$2" == "" ]
then
    sim=$(find . -type d | grep L6)
    if [ "$sim" == "" ]
    then
	cd ../
	sim=$(find . -type d | grep L6)
    fi
else
    # check whether right folder
    if [ ! -d $2 ]
    then
	echo "no sims here, go up one folder!"
	cd ..
    fi

    sim=""
    for (( i=2; i<=$#; i++ ))
    do
	echo ${!i}
	if [ -d ${!i} ]
	then
	    sim="$sim ${!i}"
	else
	    echo "simulation not found"
	fi
    done
fi

here=$PWD
echo $sim

for SIM in $sim
do
    cd $SIM
    pwd

    # check analysis steps to be done
    if [ "$1" == "prof" ]
    then
	for i in wind_hdf5_plt_cnt_???0
	do
	    wait
            echo python ../wt-analysis/wt-analysis.py $i -prof
            python ../wt-analysis/wt-analysis.py $i -prof &
        done
    fi

    if [ "$1" == "glob" ]
    then
	for i in wind_hdf5_plt_cnt_???[05]
	do
	    wait
            echo python ../wt-analysis/wt-analysis.py $i -glob
            python ../wt-analysis/wt-analysis.py $i -glob &
        done
    fi

    if [ "$1" == "warm_ion" ]
    then
	for i in wind_hdf5_plt_cnt_???[05]
	do
	    wait
            echo python ../wt-analysis/wt-analysis.py $i -warm_ion
            python ../wt-analysis/wt-analysis.py $i -warm_ion &
        done
    fi

    if [ "$1" == "cold_vol" ]
    then
	for i in wind_hdf5_plt_cnt_???[05]
	do
	    wait
            echo python ../wt-analysis/wt-analysis.py $i -cold_vol
            python ../wt-analysis/wt-analysis.py $i -cold_vol &
        done
    fi

    if [ "$1" == "CD" ]
    then
	for i in wind_hdf5_plt_cnt_??[05]0
	do
	    wait
	    echo python ../wt-analysis/wt-analysis.py $i -proj
	    python ../wt-analysis/wt-analysis.py $i -proj &
	done
    fi

    if [ "$1" == "CDall" ]
    then
	for i in wind_hdf5_plt_cnt_????
	do
	    wait
	    echo python ../wt-analysis/wt-analysis.py $i -proj
	    python ../wt-analysis/wt-analysis.py $i -proj &
	done
    fi

    if [ "$1" == "CDchemall" ]
    then
	for i in wind_hdf5_plt_cnt_????
	do
	    wait
	    echo python ../wt-analysis/wt-analysis.py $i -proj -field denshp
	    python ../wt-analysis/wt-analysis.py $i -proj -field denshp &
	    wait
	    echo python ../wt-analysis/wt-analysis.py $i -proj -field densha
	    python ../wt-analysis/wt-analysis.py $i -proj -field densha &
	    wait
	    echo python ../wt-analysis/wt-analysis.py $i -proj -field densh2
	    python ../wt-analysis/wt-analysis.py $i -proj -field densh2 &
	    wait
	    echo python ../wt-analysis/wt-analysis.py $i -proj -field dens_warm_ion
	    python ../wt-analysis/wt-analysis.py $i -proj -field dens_warm_ion &
	done
    fi

    if [ "$1" == "CDchem" ]
    then
	for i in wind_hdf5_plt_cnt_??[05]0
	do
	    wait
	    echo python ../wt-analysis/wt-analysis.py $i -proj -field denshp
	    python ../wt-analysis/wt-analysis.py $i -proj -field denshp &
	    wait
	    echo python ../wt-analysis/wt-analysis.py $i -proj -field densha
	    python ../wt-analysis/wt-analysis.py $i -proj -field densha &
	    wait
	    echo python ../wt-analysis/wt-analysis.py $i -proj -field densh2
	    python ../wt-analysis/wt-analysis.py $i -proj -field densh2 &
	done
    fi

    if [ "$1" == "CDchemtcc25" ]
    then
	for i in wind_hdf5_plt_cnt_0387
	do
	    wait
	    echo python ../wt-analysis/wt-analysis.py $i -proj -field denshp
	    python ../wt-analysis/wt-analysis.py $i -proj -field denshp &
	    wait
	    echo python ../wt-analysis/wt-analysis.py $i -proj -field densha
	    python ../wt-analysis/wt-analysis.py $i -proj -field densha &
	    wait
	    echo python ../wt-analysis/wt-analysis.py $i -proj -field densh2
	    python ../wt-analysis/wt-analysis.py $i -proj -field densh2 &
	done
    fi

    if [ "$1" == "CD_warm_ion" ]
    then
	for i in wind_hdf5_plt_cnt_0150 wind_hdf5_plt_cnt_0200 wind_hdf5_plt_cnt_0300
	do
	    wait
	    echo python ../wt-analysis/wt-analysis.py $i -proj -field dens_warm_ion
	    python ../wt-analysis/wt-analysis.py $i -proj -field dens_warm_ion &
	    #wait
	    #echo python ../wt-analysis/wt-analysis.py $i -proj -field densha
	    #python ../wt-analysis/wt-analysis.py $i -proj -field densha &
	done
    fi

    if [ "$1" == "thrmass" ]
    then
	for i in wind_hdf5_plt_cnt_???[05]
	do
	    wait
	    echo python ../wt-analysis/wt-analysis.py -threshmass $i
	    python ../wt-analysis/wt-analysis.py -threshmass $i &
	done
    fi

    if [ "$1" == "vturb" ]
    then
	for i in wind_hdf5_plt_cnt_???[05]
	do
	    wait
	    echo python ../wt-analysis/wt-analysis.py -vturb $i
	    python ../wt-analysis/wt-analysis.py -vturb $i &
	done
    fi

    if [ "$1" == "pdfchem" ]
    then
	for i in wind_hdf5_plt_cnt_???0
	do
	    wait
	    echo python ../wt-analysis/yt3-pdf-box-chem.py $i
	    python ../wt-analysis/yt3-pdf-box-chem.py $i &
	done
    fi

    cd $here
done
