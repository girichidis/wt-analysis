#!/bin/bash

cd done-L6-M00-d100-sg
file=wind_hdf5_chk_0135
python ../wt-analysis/wt-analysis.py $file -proj -field densha &
python ../wt-analysis/wt-analysis.py $file -proj -field densh2 &
python ../wt-analysis/wt-analysis.py $file -proj -field densco &
cd ..

cd done-L6-M6x-d100-sg
file=wind_hdf5_chk_0140
python ../wt-analysis/wt-analysis.py $file -proj -field densha &
python ../wt-analysis/wt-analysis.py $file -proj -field densh2 &
python ../wt-analysis/wt-analysis.py $file -proj -field densco &
cd ..

cd done-L6-M6y-d100-sg
file=wind_hdf5_chk_0175
python ../wt-analysis/wt-analysis.py $file -proj -field densha &
python ../wt-analysis/wt-analysis.py $file -proj -field densh2 &
python ../wt-analysis/wt-analysis.py $file -proj -field densco &
cd ..

