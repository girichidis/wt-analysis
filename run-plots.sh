#!/bin/bash

# check command line parameter
if [ "$1" == "" ]
then
    echo "usage:  $0  option  [sim(s)]"
    echo
    echo "options are:"
    echo "  prof      : profiles"
    echo "  CD        : coldens time evol"
    echo "  CD_3_comp : coldens time evol, compare three sims"
    echo "              here different second cmd argument, see CD_3_comp mode in plot-coldens.py"
    echo "  CD_chem_comp: compare coldens of WIM, Ha, H+"
    echo "  chem      : chem composition for different times"
    echo "  CD_paper  : reduced coldens plots for paper"
    echo "  tcc_indiv : coldens at 1.25 and 2.5 tcc (indiv. for every sim)"
    echo "  tcc       : coldens at 1.25 and 2.5 tcc (combined diff winds)"
    echo "  tcc25chem : coldens at 2.5 tcc (combined diff winds, chem composition)"
    exit
fi

# script to generate plots from the data

export PATH=/u/phigi/YT-2018-03-29/yt-conda/bin:$PATH

cd ..

if [ "$2" == "" ]
then
    sims=$(ls -d done-L6* L6*)
else
    sims=""
    for (( i=2; i<=$#; i++ ))
    do
	sims="$sims ${!i}"
    done
fi

echo $sims

# coldens comparison plots
if [ "$1" == "prof" ]
then
    for sim in $sims
    do
        echo python wt-analysis/plot-coldens.py -mode prof -sim $sim -plt $(ls $sim/wind_hdf5_plt_cnt_??00 | sed "s/.*wind_hdf5_plt_cnt_//g") -pdf
        python wt-analysis/plot-coldens.py -mode prof -sim $sim -plt $(ls $sim/wind_hdf5_plt_cnt_??00 | sed "s/.*wind_hdf5_plt_cnt_//g") -pdf
    done
fi

# coldens comparison plots
if [ "$1" == "CD" ]
then
    for sim in $sims
    do
        echo python wt-analysis/plot-coldens.py -mode time -sim $sim -plt $(ls $sim/wind_hdf5_plt_cnt_??[05]0 | sed "s/.*wind_hdf5_plt_cnt_//g") -pdf
        python wt-analysis/plot-coldens.py -mode time -sim $sim -plt $(ls $sim/wind_hdf5_plt_cnt_??[05]0 | sed "s/.*wind_hdf5_plt_cnt_//g") -pdf
    done
fi

# coldens comparison plots for three different winds
if [ "$1" == "CD_3_comp" ]
then
    for sim in $sims
    do
	pwd
        echo python wt-analysis/plot-coldens.py -mode CD_3_comp -sim $sim -plt {1..400} -pdf
        python wt-analysis/plot-coldens.py -mode CD_3_comp -sim $sim -plt {1..400} -pdf
    done
fi

# coldens comparison plots for chemical composition
if [ "$1" == "CD_chem_comp" ]
then
    for sim in $sims
    do
	pwd
        echo python wt-analysis/plot-coldens.py -mode CD_chem_comp -sim $sim -plt {1..400} -pdf
        python wt-analysis/plot-coldens.py -mode CD_chem_comp -sim $sim -plt {1..400} -pdf
    done
fi

# chemistry comparison plots
if [ "$1" == "chem" ]
then
    for sim in $sims
    do
	# first coldens plus chemical composition
	echo python wt-analysis/plot-coldens.py -mode chem -sim $sim -plt $(ls $sim/wind_hdf5_plt_cnt_?[1234]00 | sed "s/.*wind_hdf5_plt_cnt_//g") -pdf
	python wt-analysis/plot-coldens.py -mode chem -sim $sim -plt $(ls $sim/wind_hdf5_plt_cnt_?[1234]00 | sed "s/.*wind_hdf5_plt_cnt_//g") -pdf
	# then only chemical composition
	echo python wt-analysis/plot-coldens.py -mode chem -sim $sim -plt $(ls $sim/wind_hdf5_plt_cnt_?[1234]00 | sed "s/.*wind_hdf5_plt_cnt_//g") -pdf -chem_only
	python wt-analysis/plot-coldens.py -mode chem -sim $sim -plt $(ls $sim/wind_hdf5_plt_cnt_?[1234]00 | sed "s/.*wind_hdf5_plt_cnt_//g") -pdf -chem_only
    done
fi

# hand-edited plots for paper COLDENS
if [ "$1" == "CD_paper" ]
then
    # d010, plot 10 Myr 20 Myr
    python wt-analysis/plot-coldens.py -mode time -sim done-L6-M00-d010-sg -plt 0100 0200 -pdf -oname "done-L6-M00-d010-sg-two-times"
    python wt-analysis/plot-coldens.py -mode time -sim done-L6-M6x-d010-sg -plt 0100 0200 -pdf -oname "done-L6-M6x-d010-sg-two-times"
    python wt-analysis/plot-coldens.py -mode time -sim done-L6-M6y-d010-sg -plt 0100 0200 -pdf -oname "done-L6-M6y-d010-sg-two-times"

    # d050, plot 15 Myr 30 Myr
    python wt-analysis/plot-coldens.py -mode time -sim done-L6-M00-d050-sg -plt 0150 0300 -pdf -oname "done-L6-M00-d050-sg-two-times"
    python wt-analysis/plot-coldens.py -mode time -sim done-L6-M6x-d050-sg -plt 0150 0300 -pdf -oname "done-L6-M6x-d050-sg-two-times"
    python wt-analysis/plot-coldens.py -mode time -sim      L6-M6y-d050-sg -plt 0150 0300 -pdf -oname "done-L6-M6y-d050-sg-two-times"

    # d100, plot 20 Myr 40 Myr
    python wt-analysis/plot-coldens.py -mode time -sim done-L6-M00-d100-sg -plt 0200 0400 -pdf -oname "done-L6-M00-d100-sg-two-times"
    python wt-analysis/plot-coldens.py -mode time -sim done-L6-M6x-d100-sg -plt 0200 0400 -pdf -oname "done-L6-M6x-d100-sg-two-times"
    python wt-analysis/plot-coldens.py -mode time -sim done-L6-M6y-d100-sg -plt 0200 0400 -pdf -oname "done-L6-M6y-d100-sg-two-times"
fi


# coldens plots at 1.25 and 2.5 tcc
if [ "$1" == "tcc_indiv" ]
then
    # d010, plot 10 Myr 20 Myr
    python wt-analysis/plot-coldens.py -mode time -sim done-L6-M00-d010-sg -plt 0061 0122 -pdf -oname "done-L6-M00-d010-sg-two-tcc"
    python wt-analysis/plot-coldens.py -mode time -sim done-L6-M6x-d010-sg -plt 0061 0122 -pdf -oname "done-L6-M6x-d010-sg-two-tcc"
    python wt-analysis/plot-coldens.py -mode time -sim done-L6-M6y-d010-sg -plt 0061 0122 -pdf -oname "done-L6-M6y-d010-sg-two-tcc"

    # d050, plot 15 Myr 30 Myr
    python wt-analysis/plot-coldens.py -mode time -sim done-L6-M00-d050-sg -plt 0137 0273 -pdf -oname "done-L6-M00-d050-sg-two-tcc"
    python wt-analysis/plot-coldens.py -mode time -sim done-L6-M6x-d050-sg -plt 0137 0273 -pdf -oname "done-L6-M6x-d050-sg-two-tcc"
    python wt-analysis/plot-coldens.py -mode time -sim      L6-M6y-d050-sg -plt 0137 0273 -pdf -oname "done-L6-M6y-d050-sg-two-tcc"

    # d100, plot 20 Myr 40 Myr
    python wt-analysis/plot-coldens.py -mode time -sim done-L6-M00-d100-sg -plt 0193 0387 -pdf -oname "done-L6-M00-d100-sg-two-tcc"
    python wt-analysis/plot-coldens.py -mode time -sim done-L6-M6x-d100-sg -plt 0193 0387 -pdf -oname "done-L6-M6x-d100-sg-two-tcc"
    python wt-analysis/plot-coldens.py -mode time -sim done-L6-M6y-d100-sg -plt 0193 0387 -pdf -oname "done-L6-M6y-d100-sg-two-tcc"
    
fi


# coldens plots at 1.25 and 2.5 tcc, combined all plots with different winds
if [ "$1" == "tcc" ]
then
    python wt-analysis/plot-coldens.py -mode tcc -pdf
fi

# coldens plots at 1.25 and 2.5 tcc, combined all plots with different winds
if [ "$1" == "tcc25chem" ]
then
    python wt-analysis/plot-coldens.py -mode tcc25chem -pdf
fi
