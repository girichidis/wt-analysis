#!/bin/bash

Nparallel=10

function wait() {
    while [ $(ps -u $USER | grep python | wc -l) -ge $Nparallel ]; do echo "sleep"; sleep 2; done
}

export PATH=/u/phigi/YT-2018-03-29/yt-conda/bin:$PATH

function get_data() {
    wait
    echo python ../wt-analysis/wt-analysis.py $1 -proj
    python ../wt-analysis/wt-analysis.py $1 -proj &
    
    #wait
    #echo python ../wt-analysis/wt-analysis.py $1 -proj -field denshp
    #python ../wt-analysis/wt-analysis.py $1 -proj -field denshp &
    #wait
    #echo python ../wt-analysis/wt-analysis.py $1 -proj -field densha
    #python ../wt-analysis/wt-analysis.py $1 -proj -field densha &
    #wait
    #echo python ../wt-analysis/wt-analysis.py $1 -proj -field densh2
    #python ../wt-analysis/wt-analysis.py $1 -proj -field densh2 &
}

# d010
for sim in *-d010-*
do
    if [ -d $sim ]
    then
	cd $sim
	for i in wind_hdf5_plt_cnt_0061 wind_hdf5_plt_cnt_0122
	do
	    if [ -e $i ]
	    then
		get_data $i
	    fi
	done
	cd ..
    fi
done


# d050
for sim in *-d050-*
do
    if [ -d $sim ]
    then
	cd $sim
	for i in wind_hdf5_plt_cnt_0137 wind_hdf5_plt_cnt_0273
	do
	    if [ -e $i ]
	    then
		get_data $i
	    fi
	done
	cd ..
    fi
done


# d100
for sim in *-d100-*
do
    if [ -d $sim ]
    then
	cd $sim
	for i in wind_hdf5_plt_cnt_0193 wind_hdf5_plt_cnt_0387
	do
	    if [ -e $i ]
	    then
		get_data $i
	    fi
	done
	cd ..
    fi
done
