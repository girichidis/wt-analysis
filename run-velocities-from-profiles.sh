#!/bin/bash

# script to generate cloud velocities from 1D profiles
cd ..

if [ "$1" == "" ]
then
    sims=$(ls -d done-L6-M??-* L6-M??-*)
else
    sims=""
    for (( i=1; i<=$#; i++ ))
    do
	sims="$sims ${!i}"
    done
fi

echo $sims

for sim in $sims
do
    cd $sim
    python ../wt-analysis/wt-velocities-from-profiles.py $(ls wind_hdf5_plt_cnt_???0-all-profiles.dat | sed "s/-all-profiles.dat//g")
    cd ..
done
