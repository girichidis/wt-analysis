#!/bin/bash

sims=L5-*
pltbase=wind_hdf5_plt_cnt_
chkbase=wind_hdf5_chk_
prtbase=wind_hdf5_part_

# loop over all simulations
for sim in $sims
do
    # number of plot files and checkpoint files
    Nplt=$(ls ${sim}/${pltbase}???? | wc -l)
    Nprt=$(ls ${sim}/${prtbase}???? | wc -l)
    Nchk=$(ls ${sim}/${chkbase}???? | wc -l)

    size=$(du -sc $sim | awk '{print $1/(1024.0*1024.0)}')

    last_plt=$(ls ${sim}/${pltbase}???? | tail -1)
    Nblk=$(h5ls $last_plt | grep "dens " | awk '{print $3}')
    Nblkx=${Nblk:1}
    Nblk=${Nblkx%?}
    sim_time=$(h5dump -d real\ scalars $last_plt | grep -A 1 "\"time " | tail -1 | awk '{print $1/3.1536e13}')

    echo $sim $size $Nplt $Nprt $Nchk $Nblk $sim_time
done
