# script for most of the windtunnel analysis
# Philipp Girichidis, 2017-2020
 
import argparse
import yt
import numpy as np
import os

# Command line arguments
parser = argparse.ArgumentParser(description='PDF of the box (PG, Mar 2017)')
parser.add_argument('files', nargs='+', help='FLASH data files')
parser.add_argument('-field', '-f', type=str, default='density')

parser.add_argument('-lref', '-l', type=int, default=6)

# switches for the analysis
parser.add_argument('-proj',       action="store_true")
parser.add_argument('-slice',      action="store_true")
parser.add_argument('-glob',       action="store_true")
parser.add_argument('-prof',       action="store_true")
parser.add_argument('-threshmass', action="store_true")
parser.add_argument('-vturb',      action="store_true")
parser.add_argument('-pdfs',       action="store_true")
parser.add_argument('-warm_ion',   action="store_true")
parser.add_argument('-cold_vol',   action="store_true")

parser.add_argument('-force', action="store_true")
args = parser.parse_args()

mp = 1.67262177e-24
mu_mol = 2.353
mu_ato = 1.281
mu_ion = 0.612

# some hard-coded parameters
dens_threshold1 = 1e-2 * mu_ion*mp
dens_threshold2 = 1.0  * mu_ato*mp
dens_threshold3 = 1e2  * mu_ato*mp
dens_threshold4 = 1e4  * mu_mol*mp

temp_threshold1 = 50.0
temp_threshold2 = 300.0
temp_threshold3 = 8000.0
temp_warm0 = 8000.0
temp_warm1 = 3e5

###############################
# define fields for chemistry
###############################
def _Mhp(field, data):
        return (data['cell_mass'].v*data['ihp '].v)
yt.add_field('Mhp', function = _Mhp, units = "")
def _Mha(field, data):
        return (data['cell_mass'].v*data['iha '].v)
yt.add_field('Mha', function = _Mha, units = "")
def _Mh2(field, data):
        return (data['cell_mass'].v*data['ih2 '].v)
yt.add_field('Mh2', function = _Mh2, units = "")

def _denshp(field, data):
        return (data['density'].v*data['ihp '].v)
yt.add_field('denshp', function = _denshp, units = "")
def _densha(field, data):
        return (data['density'].v*data['iha '].v)
yt.add_field('densha', function = _densha, units = "")
def _densh2(field, data):
        return (data['density'].v*data['ih2 '].v)
yt.add_field('densh2', function = _densh2, units = "")


def _densco(field, data):
        return (data['density'].v*data['ico '].v)
# only add ico if all file are chk points, ico not in plots
#yt.add_field('densco', function = _densco, units = "")

def _dens_warm_ion(field, data):
        return (np.where((data['temperature'].v >= temp_warm0) & (data['temperature'].v <= temp_warm1) & (data["ihp "].v > 0.5), data['density'].v, 0.0))
yt.add_field('dens_warm_ion', function = _dens_warm_ion, units = "")



def _cold_cell_vol(field, data):
        return (np.where((data['temperature'].v <= temp_threshold2), data['cell_volume'].v, 0.0))
yt.add_field('cold_cell_vol', function = _cold_cell_vol, units = "")

def _H2_cell_vol(field, data):
        return (data['ih2 '].v*data['cell_volume'].v)
yt.add_field('H2_cell_vol', function = _H2_cell_vol, units = "")


###############################
# turbulence / kinetic energy
###############################
def _Ekin(field, data):
        return (data['cell_volume'].v*data['kinetic_energy'].v)
yt.add_field('Ekin', function = _Ekin, units = "")
def _Ekinx(field, data):
        return (0.5*data['cell_mass'].v*(data['velx'].v**2))
yt.add_field('Ekinx', function = _Ekinx, units = "")
def _Ekiny(field, data):
        return (0.5*data['cell_mass'].v*(data['vely'].v**2))
yt.add_field('Ekiny', function = _Ekiny, units = "")
def _Ekinz(field, data):
        return (0.5*data['cell_mass'].v*(data['velz'].v**2))
yt.add_field('Ekinz', function = _Ekinz, units = "")

def _sigmaxraw(field, data):
        return (data['cell_mass'].v*(data['velx'].v**2))
yt.add_field('sigmaxraw', function = _sigmaxraw, units = "")

def _velx2(field, data):
        return (data['velx'].v**2)
yt.add_field('velx2', function = _velx2, units = "")
def _vely2(field, data):
        return (data['vely'].v**2)
yt.add_field('vely2', function = _vely2, units = "")
def _velz2(field, data):
        return (data['velz'].v**2)
yt.add_field('velz2', function = _velz2, units = "")

def _velyz2(field, data):
        return (data['velz'].v**2 + data['vely'].v**2)
yt.add_field('velyz2', function = _velyz2, units = "")


###############################
# fields for thresholds
###############################

def _mass_d0(field, data):
        return (np.where((data['density'].v <= dens_threshold1), data['cell_mass'].v, 0.0))
yt.add_field('mass_d0', function = _mass_d0, units = "")
def _mass_d1(field, data):
        return (np.where((data['density'].v > dens_threshold1) & (data['density'].v <= dens_threshold2), data['cell_mass'].v, 0.0))
yt.add_field('mass_d1', function = _mass_d1, units = "")
def _mass_d2(field, data):
        return (np.where((data['density'].v > dens_threshold2) & (data['density'].v <= dens_threshold3), data['cell_mass'].v, 0.0))
yt.add_field('mass_d2', function = _mass_d2, units = "")
def _mass_d3(field, data):
        return (np.where((data['density'].v > dens_threshold3) & (data['density'].v <= dens_threshold4), data['cell_mass'].v, 0.0))
yt.add_field('mass_d3', function = _mass_d3, units = "")
def _mass_d4(field, data):
        return (np.where(data['density'].v > dens_threshold4, data['cell_mass'].v, 0.0))
yt.add_field('mass_d4', function = _mass_d4, units = "")


def _mass_T0(field, data):
        return (np.where((data['temperature'].v <= temp_threshold1), data['cell_mass'].v, 0.0))
yt.add_field('mass_T0', function = _mass_T0, units = "")
def _mass_T1(field, data):
        return (np.where((data['temperature'].v > temp_threshold1) & (data['temperature'].v <= temp_threshold2), data['cell_mass'].v, 0.0))
yt.add_field('mass_T1', function = _mass_T1, units = "")
def _mass_T2(field, data):
        return (np.where((data['temperature'].v > temp_threshold2) & (data['temperature'].v <= temp_threshold3), data['cell_mass'].v, 0.0))
yt.add_field('mass_T2', function = _mass_T2, units = "")
def _mass_T3(field, data):
        return (np.where(data['temperature'].v > temp_threshold3, data['cell_mass'].v, 0.0))
yt.add_field('mass_T3', function = _mass_T3, units = "")

def _mass_warm_ion(field, data):
        return (np.where((data['temperature'].v >= temp_warm0) & (data['temperature'].v <= temp_warm1) & (data["ihp "].v > 0.5), data['cell_mass'].v, 0.0))
yt.add_field('mass_warm_ion', function = _mass_warm_ion, units = "")


print(args.files[0][-4:])
print(args.files[-1][-4:])

# correct field string in case of chemistry
if len(args.field) < 4:
        args.field = args.field.ljust(4)
        print ("'"+args.field+"'")

for file in args.files:
        ds = yt.load(file)

        if args.pdfs:
                pdf_file = file+"-pdfs.dat"


        if args.threshmass:
                thr_file = file+"-threshold-masses.dat"
                if not os.path.isfile(thr_file) or args.force:
                        # prepare output file
                        ofile = open(thr_file, 'w')
                        ofile.write(str('#%11s' % "time"))
                        ofile.write(str('%12s'  % "totalM"))
                        ofile.write(str('%12s'  % "totalM(d0)"))
                        ofile.write(str('%12s'  % "totalM(d1)"))
                        ofile.write(str('%12s'  % "totalM(d2)"))
                        ofile.write(str('%12s'  % "totalM(d3)"))
                        ofile.write(str('%12s'  % "totalM(d4)"))
                        ofile.write(str('%12s'  % "totalM(T0)"))
                        ofile.write(str('%12s'  % "totalM(T1)"))
                        ofile.write(str('%12s'  % "totalM(T2)"))
                        ofile.write(str('%12s'  % "totalM(T3)"))
                        ofile.write("\n")
                        
                        ad = ds.all_data()
                        
                        # total masses
                        totalM, totalM_d0, totalM_d1, totalM_d2, totalM_d3, totalM_d4, totalM_T0, totalM_T1, totalM_T2, totalM_T3 = \
                        ad.quantities.total_quantity(["cell_mass", "mass_d0", "mass_d1", "mass_d2", "mass_d3", "mass_d4", "mass_T0", "mass_T1", "mass_T2", "mass_T3"])
                        
                        # write to file
                        ofile.write(str('%12.3e' % ds.current_time))
                        ofile.write(str('%12.3e' % totalM))
                        ofile.write(str('%12.3e' % totalM_d0))
                        ofile.write(str('%12.3e' % totalM_d1))
                        ofile.write(str('%12.3e' % totalM_d2))
                        ofile.write(str('%12.3e' % totalM_d3))
                        ofile.write(str('%12.3e' % totalM_d4))
                        ofile.write(str('%12.3e' % totalM_T0))
                        ofile.write(str('%12.3e' % totalM_T1))
                        ofile.write(str('%12.3e' % totalM_T2))
                        ofile.write(str('%12.3e' % totalM_T3))
                        ofile.write("\n")
                        ofile.close()


        if args.prof:

                prf_file = file+"-all-profiles.dat"
                if not os.path.isfile(prf_file) or args.force:

                        # parameter
                        ds = yt.load(file)
                        extent1 = [ds.domain_left_edge[0].in_units("pc"),ds.domain_right_edge[0].in_units("pc"),ds.domain_left_edge[1].in_units("pc"),ds.domain_right_edge[1].in_units("pc")]

                        resx = 1280
                        resy = 256
                        resz = 256
                        res = np.array((float(resx), float(resy), float(resz)))
            
                        # create covering grid
                        level = 5
                        print ("domain size:   ", ds.domain_dimensions)
                        print ("Ndims x,y,z:   ", ds.domain_dimensions * 2**level)
                        
                        L = ds.domain_width
                        dx = L/res
                        dV = dx[0]*dx[1]*dx[2]
                        
                        #print(L, dx)
                        print("extract covering grid")
                        cg = ds.covering_grid(level=level, left_edge=ds.domain_left_edge, dims=ds.domain_dimensions * 2**level)
                        print("  - density")
                        dens = cg["density"]
                        print("  - mass")
                        mass = dens*dV
                        print("  - velx")
                        velx = cg["velocity_x"]
                        print("  - vely")
                        vely = cg["velocity_y"]
                        print("  - velz")
                        velz = cg["velocity_z"]
                        print("  - magx")
                        magx = cg["magx"]
                        print("  - magy")
                        magy = cg["magy"]
                        print("  - magz")
                        magz = cg["magz"]
                        print("  - temperature")
                        temp = cg["temperature"]
                        print("  - iha")
                        iha  = cg["iha "]
                        print("  - ih2")
                        ih2  = cg["ih2 "]
                        
                        header = []
                        print()
                        
                        print("compute profiles")
                        
                        # positions
                        P_posx = np.linspace(ds.domain_left_edge[0], ds.domain_right_edge[0], num=resx, endpoint=True)
                        header.append("posx")
                        
                        # mass and volume
                        print("  - mass, volume, density")
                        P_mass = ((mass).sum(axis=2)).sum(axis=1)
                        P_volu = np.ones(resx)*dx[0]*L[1]*L[2]
                        P_dens = P_mass/P_volu
                        header.append("mass")
                        header.append("volu")
                        header.append("dens")
                        
                        # H2 mass
                        print("  - {Ha,H2} {mass,density}")
                        P_hamass = ((mass * iha).sum(axis=2)).sum(axis=1)
                        P_h2mass = ((mass * ih2).sum(axis=2)).sum(axis=1)
                        P_hadens = P_hamass/P_volu
                        P_h2dens = P_h2mass/P_volu
                        header.append("ha-mass")
                        header.append("ha-dens")
                        header.append("h2-mass")
                        header.append("h2-dens")
                        
                        # mass weighted velocities in y, z, and abs yz
                        print("  - {vx,vy,vz,vyz,vabs}")
                        P_vx_mw = ((mass*velx).sum(axis=2)).sum(axis=1)/P_mass
                        P_vy_mw = ((mass*vely).sum(axis=2)).sum(axis=1)/P_mass
                        P_vz_mw = ((mass*velz).sum(axis=2)).sum(axis=1)/P_mass
                        P_vyz_mw = ((mass*np.sqrt(vely*vely + velz*velz)).sum(axis=2)).sum(axis=1)/P_mass
                        P_v_mw   = ((mass*np.sqrt(velx*velx + vely*vely + velz*velz)).sum(axis=2)).sum(axis=1)/P_mass
                        header.append("vx_mw")
                        header.append("vy_mw")
                        header.append("vz_mw")
                        header.append("vyz_mw")
                        header.append("v_mw")
                        
                        # mass weighted magnetic field
                        print("  - {Bx,By,Bz,Babs}")
                        P_Bx_mw = ((mass*magx).sum(axis=2)).sum(axis=1)/P_mass
                        P_By_mw = ((mass*magy).sum(axis=2)).sum(axis=1)/P_mass
                        P_Bz_mw = ((mass*magz).sum(axis=2)).sum(axis=1)/P_mass
                        P_B_mw  = ((mass*np.sqrt(magx*magx + magy*magy + magz*magz)).sum(axis=2)).sum(axis=1)/P_mass
                        header.append("Bx_mw")
                        header.append("By_mw")
                        header.append("Bz_mw")
                        header.append("B_mw")
                        
                        # temperature
                        print("  - temperature")
                        P_temp = ((mass*temp).sum(axis=2)).sum(axis=1)/P_mass
                        header.append("temp")
                        
                        header_all = "#"
                        for i in range(len(header)):
                                header_all = header_all+"   ("+str(i+1).zfill(2)+")"+header[i]
                
                        # combine all profiles and write to file
                        all_profiles = (np.vstack((P_posx, P_mass, P_volu, P_dens, P_hamass, P_hadens, P_h2mass, P_h2dens, P_vx_mw, P_vy_mw, P_vz_mw, P_vyz_mw, P_v_mw, P_Bx_mw, P_By_mw, P_Bz_mw, P_B_mw, P_temp)))
                        np.savetxt(prf_file, all_profiles.T, header=header_all)

                else:
                        print(" file ", prf_file, " exists. Skipping!")
        
        ################################
        # projections, saved as npy
        ################################
        if args.proj:
                lref = args.lref
                resx = ds.domain_dimensions[0]*2**(lref-1)
                resy = ds.domain_dimensions[1]*2**(lref-1)
                resz = ds.domain_dimensions[2]*2**(lref-1)
                print(resx, resy, resz)

                field_str=""
                if args.field != "density":
                        field_str = "-"+args.field.rstrip()

                # check if coldens files exist
                res_str=str(resy).zfill(4)+"x"+str(resz).zfill(4)
                fname = file+"-projection-x-"+res_str+field_str+".npy"
                if not os.path.isfile(fname) or args.force:
                        data = np.array(yt.ProjectionPlot(ds, 'x', args.field, center=ds.domain_center).data_source.to_frb(ds.domain_width[1], (resy, resz), height=ds.domain_width[2])[args.field])
                        np.save(fname, data)
                else:
                        print("  --> "+fname+" exists! Skipping!")

                res_str=str(resx).zfill(4)+"x"+str(resz).zfill(4)
                fname = file+"-projection-y-"+res_str+field_str+".npy"
                if not os.path.isfile(fname) or args.force:
                        data = np.array(yt.ProjectionPlot(ds, 'y', args.field, center=ds.domain_center).data_source.to_frb(ds.domain_width[2], (resz, resx), height=ds.domain_width[0])[args.field])
                        np.save(fname, data)
                else:
                        print("  --> "+fname+" exists! Skipping!")

                res_str=str(resx).zfill(4)+"x"+str(resy).zfill(4)
                fname = file+"-projection-z-"+res_str+field_str+".npy"
                if not os.path.isfile(fname) or args.force:
                        data = np.array(yt.ProjectionPlot(ds, 'z', args.field, center=ds.domain_center).data_source.to_frb(ds.domain_width[0], (resx, resy), height=ds.domain_width[1])[args.field]).T
                        np.save(fname, data)
                else:
                        print("  --> "+fname+" exists! Skipping!")


        if args.vturb:
                turb_file = file+'-yz-turb.dat'
                if not os.path.isfile(turb_file) or args.force:
                        ofile = open(turb_file, 'w')
                        ofile.write(str('#%11s' % "time"))
                        ofile.write(str('%12s'  % "totalM"))
                        ofile.write(str('%12s'  % "totEkin"))
                        ofile.write(str('%12s'  % "totEkinx"))
                        ofile.write(str('%12s'  % "totEkiny"))
                        ofile.write(str('%12s'  % "totEkinz"))
                        ofile.write(str('%12s'  % "sigmax(mw)"))
                        ofile.write(str('%12s'  % "sigmax(vw)"))
                        ofile.write(str('%12s'  % "sigmay(mw)"))
                        ofile.write(str('%12s'  % "sigmay(vw)"))
                        ofile.write(str('%12s'  % "sigmaz(mw)"))
                        ofile.write(str('%12s'  % "sigmaz(vw)"))
                        ofile.write(str('%12s'  % "sigmayz(mw)"))
                        ofile.write(str('%12s'  % "sigmayz(vw)"))
                        ofile.write("\n")

                        ad = ds.all_data()

                        # total masses
                        totalM, Ekin, Ekinx, Ekiny, Ekinz = ad.quantities.total_quantity(["cell_mass", "Ekin", "Ekinx", "Ekiny", "Ekinz"])

                        #sigmax2_mw2 = ad.quantities.total_quantity(["sigmaxraw"])

                        sigmax2_mw = ad.quantities.weighted_average_quantity("velx2", "cell_mass")
                        sigmax2_vw = ad.quantities.weighted_average_quantity("velx2", "cell_volume")
                        sigmay2_mw = ad.quantities.weighted_average_quantity("vely2", "cell_mass")
                        sigmay2_vw = ad.quantities.weighted_average_quantity("vely2", "cell_volume")
                        sigmaz2_mw = ad.quantities.weighted_average_quantity("velz2", "cell_mass")
                        sigmaz2_vw = ad.quantities.weighted_average_quantity("velz2", "cell_volume")
                        sigmayz2_mw = ad.quantities.weighted_average_quantity("velyz2", "cell_mass")
                        sigmayz2_vw = ad.quantities.weighted_average_quantity("velyz2", "cell_volume")

                        ofile.write(str('%12.3e' % ds.current_time))
                        ofile.write(str('%12.3e' % totalM))
                        ofile.write(str('%12.3e' % Ekin))
                        ofile.write(str('%12.3e' % Ekinx))
                        ofile.write(str('%12.3e' % Ekiny))
                        ofile.write(str('%12.3e' % Ekinz))
                        ofile.write(str('%12.3e' % np.sqrt(sigmax2_mw)))
                        ofile.write(str('%12.3e' % np.sqrt(sigmax2_vw)))
                        ofile.write(str('%12.3e' % np.sqrt(sigmay2_mw)))
                        ofile.write(str('%12.3e' % np.sqrt(sigmay2_vw)))
                        ofile.write(str('%12.3e' % np.sqrt(sigmaz2_mw)))
                        ofile.write(str('%12.3e' % np.sqrt(sigmaz2_vw)))
                        ofile.write(str('%12.3e' % np.sqrt(sigmayz2_mw)))
                        ofile.write(str('%12.3e' % np.sqrt(sigmayz2_vw)))

                        ofile.write("\n")
                        ofile.close()

                        


        ############################
        # chem, vel, mag global
        ############################
        if args.glob and (args.force or not os.path.isfile(file+'-global.dat')):
                # prepare output file
                ofile = open(file+'-global.dat', 'w')
                ofile.write(str('#%11s' % "time"))
                ofile.write(str('%12s'  % "totalM"))
                ofile.write(str('%12s'  % "totalMHp"))
                ofile.write(str('%12s'  % "totalMHa"))
                ofile.write(str('%12s'  % "totalMH2"))
                ofile.write(str('%12s'  % "totalMHp/M"))
                ofile.write(str('%12s'  % "totalMHa/M"))
                ofile.write(str('%12s'  % "totalMH2/M"))
                ofile.write(str('%12s'  % "Vel_mw"))
                ofile.write(str('%12s'  % "Vel_vw"))
                ofile.write(str('%12s'  % "VelHp"))
                ofile.write(str('%12s'  % "VelHa"))
                ofile.write(str('%12s'  % "VelH2"))
                ofile.write(str('%12s'  % "Mag_mw"))
                ofile.write(str('%12s'  % "Mag_vw"))
                ofile.write(str('%12s'  % "MagHp"))
                ofile.write(str('%12s'  % "MagHa"))
                ofile.write(str('%12s'  % "MagH2"))
                ofile.write("\n")

                ad = ds.all_data()
        
                # total masses
                totalMHp, totalMHa, totalMH2, totalM = ad.quantities.total_quantity(["Mhp", "Mha", "Mh2", "cell_mass"])
        
                # weighted velocities
                VelM  = ad.quantities.weighted_average_quantity("velocity_magnitude", "cell_mass")
                VelV  = ad.quantities.weighted_average_quantity("velocity_magnitude", "cell_volume")
                VelHp = ad.quantities.weighted_average_quantity("velocity_magnitude", "Mhp")
                VelHa = ad.quantities.weighted_average_quantity("velocity_magnitude", "Mha")
                VelH2 = ad.quantities.weighted_average_quantity("velocity_magnitude", "Mh2")
        
                # weighted magnetic field
                MagM  = ad.quantities.weighted_average_quantity("magnetic_field_strength", "cell_mass")
                MagV  = ad.quantities.weighted_average_quantity("magnetic_field_strength", "cell_volume")
                MagHp = ad.quantities.weighted_average_quantity("magnetic_field_strength", "Mhp")
                MagHa = ad.quantities.weighted_average_quantity("magnetic_field_strength", "Mha")
                MagH2 = ad.quantities.weighted_average_quantity("magnetic_field_strength", "Mh2")

                # write to file
                ofile.write(str('%12.3e' % ds.current_time))
                ofile.write(str('%12.3e' % totalM))
                ofile.write(str('%12.3e' % totalMHp))
                ofile.write(str('%12.3e' % totalMHa))
                ofile.write(str('%12.3e' % totalMH2))
                ofile.write(str('%12.3e' % (totalMHp/totalM)))
                ofile.write(str('%12.3e' % (totalMHa/totalM)))
                ofile.write(str('%12.3e' % (totalMH2/totalM)))
                ofile.write(str('%12.3e' % VelM))
                ofile.write(str('%12.3e' % VelV))
                ofile.write(str('%12.3e' % VelHp))
                ofile.write(str('%12.3e' % VelHa))
                ofile.write(str('%12.3e' % VelH2))
                ofile.write(str('%12.3e' % MagM))
                ofile.write(str('%12.3e' % MagV))
                ofile.write(str('%12.3e' % MagHp))
                ofile.write(str('%12.3e' % MagHa))
                ofile.write(str('%12.3e' % MagH2))
                ofile.write("\n")
                ofile.close()


        ############################
        # warm ionized material
        ############################
        if args.warm_ion and (args.force or not os.path.isfile(file+'-warm-ion.dat')):
                # prepare output file
                ofile = open(file+'-warm-ion.dat', 'w')
                ofile.write(str('#%11s' % "time"))
                ofile.write(str('%12s'  % "totalM"))
                ofile.write(str('%12s'  % "totalMHp"))
                ofile.write(str('%12s'  % "totalMHa"))
                ofile.write(str('%12s'  % "totalMH2"))
                ofile.write(str('%12s'  % "totalMwi"))
                ofile.write(str('%12s'  % "totalMHp/M"))
                ofile.write(str('%12s'  % "totalMHa/M"))
                ofile.write(str('%12s'  % "totalMH2/M"))
                ofile.write(str('%12s'  % "totalMwi/M"))
                ofile.write("\n")

                ad = ds.all_data()
        
                # total masses
                totalMHp, totalMHa, totalMH2, totalM, totalM_warm_ion = ad.quantities.total_quantity(["Mhp", "Mha", "Mh2", "cell_mass", "mass_warm_ion"])
        
                # write to file
                ofile.write(str('%12.3e' % ds.current_time))
                ofile.write(str('%12.3e' % totalM))
                ofile.write(str('%12.3e' % totalMHp))
                ofile.write(str('%12.3e' % totalMHa))
                ofile.write(str('%12.3e' % totalMH2))
                ofile.write(str('%12.3e' % totalM_warm_ion))
                ofile.write(str('%12.3e' % (totalMHp/totalM)))
                ofile.write(str('%12.3e' % (totalMHa/totalM)))
                ofile.write(str('%12.3e' % (totalMH2/totalM)))
                ofile.write(str('%12.3e' % (totalM_warm_ion/totalM)))
                ofile.write("\n")
                ofile.close()


        ############################
        # cold volume
        ############################
        if args.cold_vol and (args.force or not os.path.isfile(file+'-cold-vol-.dat')):
                # prepare output file
                ofile = open(file+'-cold-vol.dat', 'w')
                ofile.write(str('#%11s' % "time"))
                ofile.write(str('%12s'  % "cold_vol"))
                ofile.write(str('%12s'  % "H2_vol"))
                ofile.write("\n")

                ad = ds.all_data()
        
                # total masses
                cold_vol, H2_vol = ad.quantities.total_quantity(["cold_cell_vol", "H2_cell_vol"])
        
                # write to file
                ofile.write(str('%12.3e' % ds.current_time))
                ofile.write(str('%12.3e' % cold_vol))
                ofile.write(str('%12.3e' % H2_vol))
                ofile.write("\n")
                ofile.close()


