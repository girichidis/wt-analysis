import numpy as np

pc    = 3.085e18
Myr   = 3.1536e13
kB    = 1.381e-16
mp    = 1.67262177e-24
gamma = 1.0
G     = 6.6743e-8
Msol  = 1.989e33
km    = 1e5
drag_coeff_sphere = 0.47
drag_coeff_cyl    = 1.15
drag_coeff_unity  = 1.00

def _sndspd(T, mu):
    return np.sqrt(gamma*kB*T/(mu*mp))

dens_a = np.array([1e-3, 1e-3, 1e-3])
temp_a = np.array([1e6, 1e6, 1e6])
molw_a = np.array([0.885, 0.885, 0.885])
csnd_a = _sndspd(temp_a, molw_a)
magn_a = np.array([1e-6, 1e-6, 1e-6])
velo_a = np.array([1e7, 1e7, 1e7])
rho_a  = dens_a*mp*molw_a
alfv_a = magn_a/np.sqrt(4.*np.pi*rho_a)
pres_a = dens_a*kB*temp_a
pmag_a = magn_a*magn_a/(8.*np.pi)

dens_c = np.array([0.1, 0.5, 1.0])
temp_c = np.array([7000.0, 1400.0, 700.0])
molw_c = np.array([1.27, 1.27, 1.27])
csnd_c = _sndspd(temp_c, molw_c)
magn_c = np.array([1e-6, 1e-6, 1e-6])
rho_c  = dens_c*mp*molw_c
alfv_c = magn_c/np.sqrt(4.*np.pi*rho_c)
pres_c = dens_a*kB*temp_a

radi_c = np.array([50.0, 50.0, 50.0])*pc


mass_c = rho_c * 4.0*np.pi/3. * np.power(radi_c,3.0)

ljeans_c = np.sqrt(gamma*kB*temp_c/(G*molw_c*mp*rho_c))
Mjeans_c = 4.*np.pi/3. * rho_c * np.power(ljeans_c/2., 3.0)

dens_contrast = dens_c/dens_a

MKHI = 2.*np.pi*radi_c*velo_a*velo_a / (G*dens_contrast)


cross_sec = np.pi*radi_c*radi_c
acc_mag       = 0.5*rho_a*(velo_a**2 + alfv_a**2)*cross_sec*drag_coeff_sphere/mass_c
acc_noB       = 0.5*rho_a*(velo_a**2)*cross_sec*drag_coeff_sphere/mass_c
acc_mag_cyl   = acc_mag*drag_coeff_cyl/drag_coeff_sphere
acc_noB_cyl   = acc_noB*drag_coeff_cyl/drag_coeff_sphere
acc_mag_unity = acc_mag*drag_coeff_unity/drag_coeff_sphere
acc_noB_unity = acc_noB*drag_coeff_unity/drag_coeff_sphere

cool_rate = 1e-25

print()
print("Wind properties")
print()
print("  wind sound speed  (km/s): ", csnd_a/1e5)
print("  wind mach number        : ", velo_a/csnd_a)
print("  wind Alfven speed (km/s): ", alfv_a/1e5)
print()


print()
print("Cloud properties")
print()



print("  number density    : ", dens_c)
print("  density           : ", rho_c)
print("  molecuar weight   : ", molw_c)
print("  temperature       : ", temp_c)
print("  radius            : ", radi_c)
print("  effective area    : ", cross_sec)
print("  sound speed       : ", csnd_c)
print("  sound speed (km/s): ", csnd_c/1e5)
print("  Mach numbers cl-w : ", velo_a/csnd_c)
print("  Alfven speed      : ", alfv_c)
print("  Alfven speed(km/s): ", alfv_c/1e5)
print("  Alfven Mach  cl-w : ", velo_a/alfv_c)
print("  Jeans length      : ", ljeans_c)
print("  Jeans mass        : ", Mjeans_c)
print("  Jeans mass (Msol) : ", Mjeans_c/Msol)
print("  KHI mass (Msol)   : ", MKHI/Msol)
print("  Mcl / KHI mass    : ", mass_c/MKHI)
print()

print("  density contrast  : ", dens_contrast)
print()
print("  pressure ratio    : ", pres_c/pres_a)
print("  plasma beta       : ", pres_a/pmag_a)
print()
print("  Cloud masses")

print("    in g   :", mass_c)
print("    in Msol:", mass_c/Msol)
print("    in MJ  :", mass_c/Mjeans_c)
print()
print("Accelerations due to drag force")
print("  Sphere")
print("    including v_A (cm/s^2)  : ", acc_mag)
print("    without v_A   (cm/s^2)  : ", acc_noB)
print("    including v_A (km/s/Myr): ", acc_mag/km*Myr)
print("    without v_A   (km/s/Myr): ", acc_noB/km*Myr)
print("  Cylinder")
print("    including v_A (cm/s^2)  : ", acc_mag_cyl)
print("    without v_A   (cm/s^2)  : ", acc_noB_cyl)
print("    including v_A (km/s/Myr): ", acc_mag_cyl/km*Myr)
print("    without v_A   (km/s/Myr): ", acc_noB_cyl/km*Myr)
print("  C_d = 1.0")
print("    including v_A (cm/s^2)  : ", acc_mag_unity)
print("    without v_A   (cm/s^2)  : ", acc_noB_unity)
print("    including v_A (km/s/Myr): ", acc_mag_unity/km*Myr)
print("    without v_A   (km/s/Myr): ", acc_noB_unity/km*Myr)
print()

print("Time scales")
print("tcc")
print(np.sqrt(dens_contrast) * radi_c/velo_a / Myr)
print("tdrag")
tdrag_nomag = dens_contrast * radi_c/velo_a / Myr
print(tdrag_nomag)
print("tdrag + mag")
tdrag_mag = dens_contrast * radi_c/velo_a * (velo_a**2/(velo_a**2 + alfv_a**2))/ Myr
print(tdrag_mag)
print("ratio tdrag (nomag-mag)/nomag")
print((tdrag_nomag-tdrag_mag)/(tdrag_nomag))
print("tff")
print(np.sqrt(3.*np.pi / (32.*G*rho_c)) / Myr)
print("sound crossing time")
#tcross_a = 2.*radi_c/csnd_a
tcross_c = 2.*radi_c/csnd_c
print(tcross_c/Myr)
print("cooling time")
tcool_c = kB*temp_c/dens_c/cool_rate
print(tcool_c/Myr)
print("growth time")
tgrow = dens_contrast * tcross_c * np.power(tcool_c/tcross_c, 0.25)
print(tgrow/Myr)
exit()


print()
print("cloud crushing time (as in Klein 1994) in Myr")
t_cc = radi_c/velo_a * np.sqrt(dens_contrast)
print(t_cc / Myr)
print("simulation time in units of the cloud crushing time")
print(40.0 * Myr / t_cc)
print("simulation at 1.25 and 2.5 tcc")
print(1.25*t_cc/Myr)
print(2.5*t_cc/Myr)
print()

print("bow shock formation time (Myr)")
tau_bs = 2.0*radi_c/velo_a
print(tau_bs/Myr)

print()

print("cloud sound speed (km/s)")
print(csnd_c/1e5)

print()
print("crushing shock by ram pressure, v_cs (km/s)")
v_cs = velo_a/np.sqrt(dens_contrast)
print(v_cs/1e5)

print()
print("crushing shock by ram pressure, tau_cs (Myr)")
print(radi_c/csnd_c/Myr)

print()
print("crushing time scale tau_cr (Myr)")
print(2.0*radi_c / v_cs/Myr)

print()
print("slow-down (speed-up) time scale (Myr)")
tau_dr = 3./4.*dens_contrast*radi_c/velo_a
print(tau_dr/Myr)
