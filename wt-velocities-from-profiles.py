# script for the analysis of velocities based on density profiles
# Philipp Girichidis, 2019-02

# this script analysis the 1D profiles along the x-axis, finds positions like
# the 25 and 50 percentile or the centre of mass and the corresponding 
# velocity

import argparse
import yt
import numpy as np
import os

pc = 3.085e18

# Command line arguments
parser = argparse.ArgumentParser(description='PDF of the box (PG, Mar 2017)')
parser.add_argument('files', nargs='+', help='FLASH data files')
args = parser.parse_args()

times = []
positions  = []
cloud_endp = []
cloud_mass = []
cloud_Mh2  = []

fraction=[0.25, 0.4, 0.5, 0.6]

for file in args.files:

    # load the hdf5 file to get time
    ds   = yt.load(file)
    times.append(ds.current_time.v)

    # load precomputed profile file
    prf  = np.loadtxt(file+"-all-profiles.dat")

    # get total mass of the profile
    Mtot = prf[:,1].sum()

    # the centre of mass
    CoMx = (prf[:,0]*prf[:,1]).sum()/Mtot

    # and the cumulative mass profile
    cum_rel_mass = prf[:,1].cumsum()/Mtot

    # create array with positions
    # first N are the cumulative mass fractions
    position=[0.0]*(1+len(fraction))
    for f, i in zip(fraction, range(len(fraction))):
        idx = np.argwhere(cum_rel_mass > f)[0]
        position[i] = prf[idx,0][0]
    # last entry is centre of mass
    position[len(fraction)] = CoMx

    # append positions to global array
    positions.append(position)

    # compute cloud mass as integrated mass
    # up to 25th percentile + 100 pc
    idx25 = np.argwhere(cum_rel_mass > 0.25)[0]
    pos25 = prf[idx25,0][0]
    pos25p100 = pos25+100.*pc
    idx25p100 = np.argwhere(prf[:,0] > pos25p100)[0]
    print(idx25, pos25, pos25p100, idx25p100)
    cloud_mass_tot = prf[0:idx25p100[0],1].sum()
    cloud_mass_h2  = prf[0:idx25p100[0],6].sum()
    print(prf[idx25p100,0][0], cloud_mass_tot)
    cloud_endp.append(prf[idx25p100,0][0])
    cloud_mass.append(cloud_mass_tot)
    cloud_Mh2.append(cloud_mass_h2)

# done with all files, compute velocities
np_times = np.asarray(times)
np_positions = np.asarray(positions)
np_cloud_endp = np.asarray(cloud_endp)
np_cloud_mass = np.asarray(cloud_mass)
np_cloud_Mh2  = np.asarray(cloud_Mh2)

mid_times  = 0.5*(np_times[1:]+np_times[0:-1])
del_times  = (np_times[1:]-np_times[0:-1])

velocities = (np_positions[1:,:]-np_positions[0:-1,:])
for i in range(velocities.shape[1]):
    velocities[:,i] = velocities[:,i]/del_times

# store positions of percentiles and CoM_x
# also store end of cloud and cloud mass (tot, H2)
data = np_times
header="time"
for i in range(np_positions.shape[1]):
    data = np.vstack((data, np_positions[:,i]))
    if i < len(fraction):
        header=header+"   posx(Mfrac="+str(fraction[i])+")"
    else:
        header=header+"   posx(CoM_x)"
data = np.vstack((data, np_cloud_endp))
header=header+"   posx(cloudend)"
data = np.vstack((data, np_cloud_mass))
header=header+"   cloud_mass_tot"
data = np.vstack((data, np_cloud_Mh2))
header=header+"   cloud_mass_h2"
np.savetxt("global-positions-from-profiles.dat", data.T, header=header)

# store corresponding velocities
data = mid_times
header="time"
for i in range(velocities.shape[1]):
    data = np.vstack((data, velocities[:,i]))
    if i < len(fraction):
        header=header+"   velo(Mfrac="+str(fraction[i])+")"
    else:
        header=header+"   velo(CoM_x)"
np.savetxt("global-velocites-from-profiles.dat", data.T, header=header)

