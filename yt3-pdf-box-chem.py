# script to find density pdfs for entire box
# Philipp Girichidis, 2016, Feb 23
 
import argparse
import matplotlib
matplotlib.use('Agg')
import yt
from matplotlib.colors import LogNorm
import numpy as np
#import SILCC_functions
import ntpath

kpc = 3.085e21

T1=3e5
T2=8000.0
T3=300.0
T4=30.

# temperature cuts
def _MT1(field, data):
        return np.where(data["temp"].v >= T1, data['cell_mass'], 0.0)
yt.add_field('MT1', function = _MT1, units = "g")

def _MT2(field, data):
        return np.where((data["temp"].v >= T2) & (data["temp"].v < T1), data['cell_mass'], 0.0)
yt.add_field('MT2', function = _MT2, units = "g")
		 
def _MT3(field, data):
        return np.where((data["temp"].v >= T3) & (data["temp"].v < T2), data['cell_mass'], 0.0)
yt.add_field('MT3', function = _MT3, units = "g")

def _MT4(field, data):
        return np.where((data["temp"].v >= T4) & (data["temp"].v < T3), data['cell_mass'], 0.0)
yt.add_field('MT4', function = _MT4, units = "g")

def _MT5(field, data):
        return np.where((data["temp"].v < T4), data['cell_mass'], 0.0)
yt.add_field('MT5', function = _MT5, units = "g")

def _VT1(field, data):
        return np.where((data["temp"].v >= T1), data['cell_volume'], 0.0)
yt.add_field('VT1', function = _VT1, units = "cm**3")

def _VT2(field, data):
        return np.where((data["temp"].v >= T2) & (data["temp"].v < T1), data['cell_volume'], 0.0)
yt.add_field('VT2', function = _VT2, units = "cm**3")

def _VT3(field, data):
        return np.where((data["temp"].v >= T3) & (data["temp"].v < T2), data['cell_volume'], 0.0)
yt.add_field('VT3', function = _VT3, units = "cm**3")

def _VT4(field, data):
        return np.where((data["temp"].v >= T4) & (data["temp"].v < T3), data['cell_volume'], 0.0)
yt.add_field('VT4', function = _VT4, units = "cm**3")

def _VT5(field, data):
	return np.where(data["temp"].v < T4, data['cell_volume'], 0.0)
yt.add_field('VT5', function = _VT5, units = "cm**3")


#chemical species
def _Mhp(field, data):
        return (data['cell_mass'].v*data['ihp '].v)
yt.add_field('Mhp', function = _Mhp, units = "")

def _Mha(field, data):
        return (data['cell_mass'].v*data['iha '].v)
yt.add_field('Mha', function = _Mha, units = "")
 
def _Mh2(field, data):
        return (data['cell_mass'].v*data['ih2 '].v)
yt.add_field('Mh2', function = _Mh2, units = "")


def _Vhp(field, data):
        return (data['cell_volume'].v*data['ihp '].v)
yt.add_field('Vhp', function = _Vhp, units = "")

def _Vha(field, data):
        return (data['cell_volume'].v*data['iha '].v)
yt.add_field('Vha', function = _Vha, units = "")
 
def _Vh2(field, data):
        return (data['cell_volume'].v*data['ih2 '].v)
yt.add_field('Vh2', function = _Vh2, units = "")

Newton=6.67408e-8
# free-fall time
def _free_fall_time(field, data):
        return (np.sqrt(3.*np.pi/(32.*Newton*data['density'].v)))
yt.add_field('free_fall_time', function = _free_fall_time, units = "")




# Command line arguments
parser = argparse.ArgumentParser(description='PDF of the box (PG, Mar 2017)')
parser.add_argument('files', nargs='+', help='FLASH data files')
parser.add_argument('-nbins', type=int, default=100)
parser.add_argument('-fmin', type=float, default=1e-28)
parser.add_argument('-fmax', type=float, default=1e-18)
parser.add_argument('-field', '-f', type=str, default='density')
parser.add_argument('-wfield', '-wf', type=str, default=None)
parser.add_argument('-profile', '-p', type=str, default='cell_volume')

# those parameters should be the same for all scripts
parser.add_argument('-idir', default=".", type=str)
parser.add_argument('-odir', default=".", type=str)

args = parser.parse_args()

Profile_x = np.zeros(args.nbins, dtype=np.float64)

# collects all profiles, 3D array: 1=files, 2=Nfields=4(tot+3chem), 3=Nbins
Profiles = np.zeros((len(args.files),4,args.nbins), dtype=np.float64)
print (Profiles.shape)

first_profile = True
file_cnt=-1
ofile=args.files[0]
for pathfile in args.files:

        file=ntpath.basename(pathfile)
        print ("load file")
        pf = yt.load(args.idir+'/'+file)

        ad = pf.all_data()
        file_cnt=file_cnt+1

        if file_cnt==0:
                header=str('%16s' % "time")
                header=header+str('%17.5e'% pf.current_time)+"\n"
                header=header+str('%22s' % "x")
                header=header+str('%25s' % "PDF_avg")
                header=header+str('%25s' % "PDF_stddev")

                prof = yt.Profile1D(ad, args.field, args.nbins, args.fmin, args.fmax, True, weight_field=args.wfield)
        if first_profile == True:
                Profile_x = prof.x
                first_profile = False
                print (Profile_x)
        if args.profile == "cell_volume":
                prof_hp = "Vhp"
                prof_ha = "Vha"
                prof_h2 = "Vh2"
        elif args.profile == "cell_mass":
                prof_hp = "Mhp"
                prof_ha = "Mha"
                prof_h2 = "Mh2"
        else:
                print("This version of the script only works for cell_mass or cell_volume! Try the version without chem for other fields!")
                exit()
        prof.add_fields(args.profile)
        prof.add_fields(prof_hp)
        prof.add_fields(prof_ha)
        prof.add_fields(prof_h2)
        Profiles[file_cnt,0] = np.divide(prof[args.profile].v, prof[args.profile].v.sum())
        Profiles[file_cnt,1] = np.divide(prof[prof_hp].v,      prof[args.profile].v.sum())
        Profiles[file_cnt,2] = np.divide(prof[prof_ha].v,      prof[args.profile].v.sum())
        Profiles[file_cnt,3] = np.divide(prof[prof_h2].v,      prof[args.profile].v.sum())

# now all profiles have been computed
# compute mean and scatter (1 for x, 2 for mean/stddev times 4 (tot+3chem))
results = np.zeros((1+2*4,args.nbins), dtype=np.float64)
results[0] = Profile_x
results[1] = np.mean(Profiles[:,0,:], axis=0)
results[2] = np.std (Profiles[:,0,:], axis=0)
results[3] = np.mean(Profiles[:,1,:], axis=0)
results[4] = np.std (Profiles[:,1,:], axis=0)
results[5] = np.mean(Profiles[:,2,:], axis=0)
results[6] = np.std (Profiles[:,2,:], axis=0)
results[7] = np.mean(Profiles[:,3,:], axis=0)
results[8] = np.std (Profiles[:,3,:], axis=0)

np.savetxt(args.odir+"/"+ofile+"-pdfs-chem-"+args.field+"-"+args.profile+".dat", results.T, header=header)
